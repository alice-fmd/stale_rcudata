dnl -*- mode: Autoconf -*- 
dnl
dnl  
dnl  ROOT generic rcudata framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl AC_RCUDATA([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_RCUDATA],
[
    AC_REQUIRE([ROOT_PATH])
    # Command line argument to specify prefix. 
    AC_ARG_WITH([rcudata-prefix],
        [AC_HELP_STRING([--with-rcudata-prefix],
		[Prefix where RcuData is installed])],
        rcudata_prefix=$withval, rcudata_prefix="")

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([rcudata-url],
        [AC_HELP_STRING([--with-rcudata-url],
		[Base URL where the RcuData dodumentation is installed])],
        rcudata_url=$withval, rcudata_url="")
    if test "x${RCUDATA_CONFIG+set}" != xset ; then 
        if test "x$rcudata_prefix" != "x" ; then 
	    RCUDATA_CONFIG=$rcudata_prefix/bin/rcudata-config
	fi
    fi   

    # Check for the configuration script. 
    AC_PATH_PROG(RCUDATA_CONFIG, rcudata-config, no)
    rcudata_min_version=ifelse([$1], ,0.11,$1)
    
    # Message to user
    AC_MSG_CHECKING(for RcuData version >= $rcudata_min_version)

    # Check if we got the script
    rcudata_found=no    
    if test "x$RCUDATA_CONFIG" != "xno" ; then 
       # If we found the script, set some variables 
       RCUDATA_CPPFLAGS=`$RCUDATA_CONFIG --cppflags`
       RCUDATA_INCLUDEDIR=`$RCUDATA_CONFIG --includedir`
       RCUDATA_LIBS=`$RCUDATA_CONFIG --libs`
       RCUDATA_LTLIBS=`$RCUDATA_CONFIG --ltlibs`
       RCUDATA_LIBDIR=`$RCUDATA_CONFIG --libdir`
       RCUDATA_LDFLAGS=`$RCUDATA_CONFIG --ldflags`
       RCUDATA_LTLDFLAGS=`$RCUDATA_CONFIG --ltldflags`
       RCUDATA_PREFIX=`$RCUDATA_CONFIG --prefix`
       RCUDATA_READER=`$RCUDATA_CONFIG --have-reader`
       RCUDATA_LOWLEVEL=`$RCUDATA_CONFIG --have-lowlevel`

       # Check the version number is OK.
       rcudata_version=`$RCUDATA_CONFIG -V` 
       rcudata_vers=`echo $rcudata_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       rcudata_regu=`echo $rcudata_min_version | \
         awk 'BEGIN { FS = "."; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $rcudata_vers -ge $rcudata_regu ; then 
            rcudata_found=yes
       fi
    fi
    AC_MSG_RESULT($rcudata_found - is $rcudata_version) 

    # Some autoheader templates. 
    AH_TEMPLATE(HAVE_RCUDATA_CHANNEL_H, 
                [Whether we have rcudata/Channel.h header])
    AH_TEMPLATE(HAVE_RCUDATA, [Whether we have rcudata])


    if test "x$rcudata_found" = "xyes" ; then
        # Now do a check whether we can use the found code. 
        save_LDFLAGS=$LDFLAGS
	save_CPPFLAGS=$CPPFLAGS
    	LDFLAGS="$LDFLAGS -L$RCUDATA_LIBDIR $RCUDATA_LIBS -L$ROOTLIBDIR $ROOTLIBS $ROOTAUXLIBS"
    	CPPFLAGS="$CPPFLAGS $RCUDATA_CPPFLAGS $ROOTCFLAGS"
 
        # Change the language 
        AC_LANG_PUSH(C++)

	# Check for a header 
        have_rcudata_channel_h=0
        AC_CHECK_HEADER([rcudata/Channel.h], [have_rcudata_channel_h=1])

        # Check the library. 
        have_librcudata=no
        AC_MSG_CHECKING(for -lrcudata)
        AC_LINK_IFELSE([
        AC_LANG_PROGRAM([#include <rcudata/Channel.h>],
                        [new RcuData::Channel;])], 
                        [have_librcudata=yes])
        AC_MSG_RESULT($have_librcudata)

        if test $have_rcudata_channel_h -gt 0    && \
            test "x$have_librcudata"   = "xyes" ; then

            # Define some macros
            AC_DEFINE(HAVE_RCUDATA)
        else 
            rcudata_found=no
        fi
        # Change the language 
        AC_LANG_POP(C++)
    fi

    AC_MSG_CHECKING(where the RcuData documentation is installed)
    if test "x$rcudata_url" = "x" && \
	test ! "x$RCUDATA_PREFIX" = "x" ; then 
       RCUDATA_URL=${RCUDATA_PREFIX}/share/doc/rcudata/html
    else 
	RCUDATA_URL=$rcudata_url
    fi	
    AC_MSG_RESULT($RCUDATA_URL)
   
    if test "x$rcudata_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(RCUDATA_URL)
    AC_SUBST(RCUDATA_PREFIX)
    AC_SUBST(RCUDATA_CPPFLAGS)
    AC_SUBST(RCUDATA_INCLUDEDIR)
    AC_SUBST(RCUDATA_LDFLAGS)
    AC_SUBST(RCUDATA_LIBDIR)
    AC_SUBST(RCUDATA_LIBS)
    AC_SUBST(RCUDATA_LTLIBS)
    AC_SUBST(RCUDATA_LTLDFLAGS)
    AC_SUBST(RCUDATA_READER)
    AC_SUBST(RCUDATA_LOWLEVEL)
])

dnl
dnl EOF
dnl 
