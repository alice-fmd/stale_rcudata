// -*- mode: c++ -*-  
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:04:26 2006
    @brief   Base class for data acquisition - declaration
*/
#ifndef RCUDATA_PROGRESSMETER_H
#define RCUDATA_PROGRESSMETER_H
#ifndef __CTIME__
# include <ctime>
#endif
#ifndef __STRING__
# include <string>
#endif

namespace RcuData 
{
  /** @class ProgressMeter rcudata/ProgressMeter.h <rcudata/ProgressMeter.h>
      @ingroup rcudata_util
   */
  class ProgressMeter
  {
  public:
    /** Constructor 
	@param n Number of iterations
	@param freq Update frequency */
    ProgressMeter(int n=0, int freq=10);
    /** Step on iteration */
    void Step();
    /** Reset progress bar to new number of iterations 
	@param n New number of iterations 
	@param pre Prefix string 
	@param freq Frequency */
    void Reset(int n, const char* pre="Event #", int freq=0);
    /** @return  The current count */
    unsigned long Current() const { return fCurrent; }
    /** Set the prefix 
	@param pre Prefix to use */
    void SetPrefix(const char* pre) { fPre = pre; }
    /** Set the frequency 
	@param freq New frequency */
    void SetFreq(int freq=1) { fFreq = (freq <= 0 ? fFreq : freq); }
    /** Update the total number */
    void SetN(unsigned long n) { fN = n; }
    /** Get the total number */
    unsigned long N() const { return fN; }
    /** Set whether we should finish each line with a newline 
	(@a dump==true) or a carriage return (@a dump==false).  This
	is usefull when redirecting standard out to a file 
	@param dump If true, finish with a new line */
    void SetDump(bool dump=true);
    /** @return@c true if we're outputting to a file.  Useful for
	client programs to reset to a different frequency */
    bool IsDump() const { return fDump; }
  private:
    size_t PrintPrefix();
    size_t PrintBar();
    size_t PrintPercentage();
    size_t PrintEta();
    size_t PrintThrobber();
    /** When we started */
    time_t fStart;
    /** Now */
    time_t fNow;
    /** Number of iterations */
    int    fN;
    /** Current iteration */
    unsigned long fCurrent;
    /** Write out frequency */
    int    fFreq;
    /** Current throbber character */
    char   fThrobber;
    /** Pre-string */
    std::string fPre;
    /** Whether we should terminate with a carriage return or a newline */
    bool fDump;
  };  
}

#endif
//
// EOF
//
