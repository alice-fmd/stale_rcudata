// -*- mode: c++ -*-
//
//   ROOT generic framework
//   Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    rcudata/LinkDef.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Thu Dec 16 00:20:57 2004
    @brief   Linkage specifications 
*/
#ifndef __CINT__
#error Not for compilation
#endif

#pragma link off all functions;
#pragma link off all globals;
#pragma link off all classes;

#pragma link C++ namespace RcuData;
#pragma link C++ class     RcuData::Channel+;
#pragma link C++ class     RcuData::AltroDecoder;
#pragma link C++ class     RcuData::ProgressMeter;
#pragma link C++ class     RcuData::ChannelVisitor;
#pragma link C++ class     RcuData::SpectrumMaker;
#if 0
#pragma link C++ namespace RcuData::Spectra;
#pragma link C++ class     RcuData::Spectra::Container<Board>+;
#pragma link C++ class     RcuData::Spectra::Container<Chip>+;
#pragma link C++ class     RcuData::Spectra::Container<Chan>+;
#pragma link C++ class     RcuData::Spectra::Container<TH1>+;
#pragma link C++ class     RcuData::Spectra::Rcu+;
#pragma link C++ class     RcuData::Spectra::Board+;
#pragma link C++ class     RcuData::Spectra::Chip+;
#pragma link C++ class     RcuData::Spectra::Chan+;
#endif

//____________________________________________________________________ 
//  
// EOF
//
