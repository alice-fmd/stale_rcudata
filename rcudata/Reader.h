// -*- mode: C++ -*-
//____________________________________________________________________
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Base class for reading channel information.
*/
#ifndef RCUDATA_READER_H
#define RCUDATA_READER_H
#include <cctype>
#include <rcudata/Types.h>

class TClonesArray;

namespace RcuData
{
  struct Channel;
  
  /** Abstract visitor of ALTRO events. 
      The user should derive her class from this class, and over load
      the member functions GotChannel and GotData.  For example 

      @code 
      struct MyVisitor : public ChannelVisitor 
      {
         bool GotChannel(Channnel& c) 
	 { 
	   TH1*    h = GetChannelHistogram(c);
	   if (!h) h = MakeChannelHistogram(c);
	   fChanHist = h;
	   fChannel  = &c;
	   return true;
	 }
	 bool GotData(unsigned t, unsigned adc) 
	 {
	   TH1*    h = GetTimebinHistogram(fChannel, t);
	   if (!h) h = MakeTimebinHistogram(fChannel, t);
	   h->Fill(adc);
	   fChanHist->Fill(adc);
	   return true;
         }
	 TH1* GetChannelHistogram(Channel* c);
	 TH1* MakeChannelHistogram(Channel* c);
	 TH1* GetTimebinHistogram(Channel* c, unsigned t);
	 TH1* MakeTimebinHistogram(Channel* c, unsigned t);
	 Channel* fChannel;
	 TH1*     fChanHist;
	 // ... more stuff ... 
       };
       @endcode 
   */
  struct ChannelVisitor 
  {
    /** Destructor */
    virtual ~ChannelVisitor() {}
    /** This is called when we get a new channel.  Note, that the data
	for the channel may not be available at this time. 
	@param c The next channel 
	@param hasData @c true if the passed channel object has data. 
	@return If @c false is returned, this channel is not processed
	any further. */ 
    virtual bool GotChannel(Channel& c, bool hasData) = 0;
    /** This is called when we get an ADC value for the current
	channel. 
	@param t Time bin number 
	@param adc ADC value 
	@return If @c false is turned, this channel is not processed
	any further. */
    virtual bool GotData(uint32_t t, uint32_t adc) = 0;
    /** Called when all channel information is available at the end of
	an event. 
	@param channels Array of Channel object. */
    virtual void GotEvent(TClonesArray& channels) {}
  };
  
  /** Abstract reader of ALTRO events. 
      Use the static member function Create to make a reader of the
      appropriate type, passing it a ChannelVisitor object and the
      source to read from.  Then use the member function GetNextEvent
      to read the data in and let the ChannelVisitor object process
      the data on the fly.  For example: 
      @code 
      struct MyReader : public ChannelVisitor
      { 
        MyReader(const char* src, long n, size_t skip) 
	{
	   fReader = Reader::Create(*this, src, n, skip);
	   if (fReader) return;
	   std::cerr << "No reader made!" << std::endl;
	   exit(1);
	}
	void Exec()
	{
	   while (fReader->GetNextEvent()) {
	     // do something after processing all channels 
	   }
        }
        bool GotChannel(Channnel& c) 
	{ 
	  TH1*    h = GetChannelHistogram(c);
	  if (!h) h = MakeChannelHistogram(c);
	  fChanHist = h;
	  fChannel  = &c;
	  return true;
	}
	bool GotData(unsigned t, unsigned adc) 
	{
	  TH1*    h = GetTimebinHistogram(fChannel, t);
	  if (!h) h = MakeTimebinHistogram(fChannel, t);
	  h->Fill(adc);
	  fChanHist->Fill(adc);
	  return true;
        }	
	Reader* fReader;
	// ... more stuff ... 
	static int main(int argc, char** argv) 
	{ 
	  long        n    = -1;
	  size_t      skip = 0;
	  std::string src;
	  
	  for (size_t i = 1; i < argc; i++) {
	    if (argv[i][0] != '-') {
	      src = argv[i];
	      continue;
	    }
	    switch (argv[i][1]) {
	    case 'h': 
              std::cout << "Usage: " << argv[0] << " [OPTIONS] INPUT"
	                << std::endl;
	      break;
            case 'n': { 
	      std::stringstreams s(argv[i+1]); 
	      s >> n;
	      i++;
	      }
	      break;
            case 's': { 
	      std::stringstreams s(argv[i+1]); 
	      s >> skip;
	      i++;
	      }
	      break;
	    }
          }
	  
	  MyReader r(src.c_str(), n, skip);
	  r.Exec();
          return 0;
        };

	int main(int argc, char** argv) 
        {
	  return MyReader::main(argc, argv);
	}
	@endcode 

   */
  struct Reader
  {
    enum {
      kData,
      kSkip,
      kNoData, 
      kEndOfData, 
      kMaxReached,
      kError
    };
    
    /** Static member function to make a reader 
	@param v    Visitor 
	@param src  The input 
	@param n    Number of events to read. 
	@param skip The number of events to skip. 
	@param tree If @c true, make an output tree
	@param all  Ask for all events, not just some.
	@param block Block read
	@return a newly allocated reader, or 0 on error */
    static Reader* Create(ChannelVisitor& v, 
			  const char*     src, 
			  long            n=-1, 
			  unsigned int    skip=0, 
			  bool            tree=true, 
			  bool            all=true, 
			  bool            block=false);

    /** Destructor */
    virtual ~Reader() {}
    /** Read in next event.  
	@return @c true on success, @c false otherwise */
    virtual int GetNextEvent() = 0;
    /** @return the (estimated) number of events */
    virtual long GetNumberOfEvents() const { return fMaxEvents; }
    /** Get the last read event number */ 
    virtual long GetEventNumber() const { return fCurrentEvent; }
    /** @return end of data flag */
    bool IsEOD() const { return fIsEOD; }
  protected:
    /** Contructor. */
    Reader(ChannelVisitor& v, 
	   long         n      = -1, 
	   unsigned int skip   = 0) 
      : fProc(v), 
	fMaxEvents(n), 
	fSkipEvents(skip), 
	fCurrentEvent(0), 
	fIsEOD(true)
    {}    
    /** Set input source.
	@param src Where to read from. 
	@return @c true on succes, @c false otherwise */
    virtual bool SetInput(const char* src, bool, bool) { return false; }
    /** The channel visitor */ 
    ChannelVisitor& fProc;
    /** Number of events to read */
    long fMaxEvents;
    /** Number of events to skip */
    unsigned int fSkipEvents;
    /** Current event number */
    unsigned long fCurrentEvent;
    /** Whether we're at the end of the data */
    bool fIsEOD;
  };
}

#endif
//
// EOF
//

  
