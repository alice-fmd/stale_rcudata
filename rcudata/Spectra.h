// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Spectra.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:08 2006
    @brief   
    @ingroup rcudata_data        
*/
#ifndef RCUDATA_SPECTRA
#define RCUDATA_SPECTRA
#ifndef RCUDATA_CONTAINER
# include <rcudata/Container.h>
#endif
#ifndef __MAP__
# include <map>
#endif
#ifndef __SSTREAM__
# include <sstream>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif
#ifndef ROOT_TDirectory
# include <TDirectory.h>
#endif
#ifndef ROOT_TH1
# include <TH1.h>
#endif
class TH1;
class TH1D;
class TH1F;
class TObjArray;
class TTree;
class TClonesArray;

namespace RcuData 
{
  /** @defgroup rcudata_spectra Cache of spectra 
      @ingroup rcudata 
  */

  /** @namespace Spectra Namespace for Cache of spectra 
      @ingroup rcudata_spectra 
  */
  namespace Spectra 
  {
    // Forward declarations 
    struct Rcu;
    struct Board;
    struct Chip;
    struct Chan;
    /** Container of rcus */
    typedef Container<Rcu> RcuCont;
    /** Container of boards */
    typedef Container<Board> BoardCont;
    /** Container of chips */
    typedef Container<Chip>  ChipCont;
    /** Container of channels */
    typedef Container<Chan>  ChanCont;
    /** Container of histograms */
    typedef Container<TH1>   HistCont;
    
    //__________________________________________________________________
    /** Container of Rcu information 
	@ingroup rcudata_spectra
    */
    struct Top : public Container<Rcu>
    {
      /** Type of base class */
      typedef Container<Rcu> Base_t;
      /** Constructor */
      Top(unsigned int id=0);
      /** @return Get name */
      const char* GetName() const;
      /** Get a board, or if it doesn't exist, make a new one. 
	  @param id Id of rcu 
	  @return Pointer to rcu containeer */
      Rcu* GetOrAdd(unsigned int id);
      /** Put information in file */
      void WriteOut();
      /** Fill histograms 
	  @param rcu      RCU number
	  @param board    Board number
	  @param chip     Chip number
	  @param channel  Channel number 
	  @param t        Time bin number
	  @param adc      ADC Value */
      virtual void Fill(unsigned int rcu,  unsigned int board,   
			unsigned int chip, unsigned int channel, 
			unsigned int t,    unsigned int adc);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      // ClassDef(Rcu,1);
    };

    //__________________________________________________________________
    /** Container of Rcu information 
	@ingroup rcudata_spectra
    */
    struct Rcu : public Container<Board>
    {
      /** Type of base class */
      typedef Container<Board> Base_t;
      /** Constructor */
      Rcu(unsigned int id, Top& top);
      /** @return Get name */
      const char* GetName() const;
      /** Get a board, or if it doesn't exist, make a new one. 
	  @param id Id of board 
	  @return Pointer to board containeer */
      Board* GetOrAdd(unsigned int id);
      /** Fill histograms 
	@param board    Board number
	@param chip     Chip number
	@param channel  Channel number 
	@param t        Time bin number
	@param adc      ADC Value */
      virtual void Fill(unsigned int board,   unsigned int chip, 
			unsigned int channel, unsigned int t,     
			unsigned int adc);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      Top* fTop;
      // ClassDef(Rcu,1);
    };

    //__________________________________________________________________
    /** Container of Board information 
	@ingroup rcudata_spectra
    */
    struct Board : public Container<Chip>
    {
      /** Type of base class */
      typedef Container<Chip> Base_t;
      /** Constructor */
      Board() {}
      /** Constructor 
	  @param id   Board number
	  @param rcu Controlling RCU */
      Board(unsigned int id, Rcu& rcu);
      /** @return Get name */
      const char* GetName() const;
      /** Get parent */
      const Rcu& Mother() const { return *fRcu; }
      /** Get a chip, or if it doesn't exist, make a new one. 
	  @param id Id of chip 
	  @return Pointer to chip containeer */
      Chip* GetOrAdd(unsigned int id);
      /** Fill histograms 
	@param chip     Chip number
	@param channel  Channel number 
	@param t        Time bin number
	@param adc      ADC Value */
      virtual void Fill(unsigned int chip, unsigned int channel, 
			unsigned int t,    unsigned int adc);
    protected: 
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      /** Up link */
      Rcu* fRcu;
      // ClassDef(Board,1);
    };
    

    //__________________________________________________________________
    /** Container of Chip information 
	@ingroup rcudata_spectra
    */
    struct Chip : public Container<Chan>
    {
      /** Type of base class */
      typedef Container<Chan> Base_t;
      /** Constructor */
      Chip() {}
      /** Constructor 
	  @param n Id of chip 
	  @param b Back link to board */
      Chip(unsigned int n, Board& b);
      /** @return Get name */
      const char* GetName() const;
      /** @return reference to mother board */
      const Board& Mother() const { return *fBoard; }
      /** Get a channel, or if it doesn't exist, make a new one. 
	  @param id Id of chan 
	  @return Pointer to chan containeer */
      Chan* GetOrAdd(unsigned int id);
      /** Fill histograms 
	  @param channel  Channel number 
	  @param t        Time bin number
	  @param adc      ADC Value */
      virtual void Fill(unsigned int channel, unsigned int t,unsigned int adc);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This is used for the
	  final progress bar. 
	  @param e element to do counting for 
	  @return Total count of elements in @a e and it's daughters */
      virtual size_t CountElem(Elem_t* e) const;
      /** Back link to board */
      Board* fBoard;
      // ClassDef(Chip,1);
    };

    //__________________________________________________________________
    /** Container of Channel information 
	@ingroup rcudata_spectra
    */
    struct Chan : public Container<TH1>
    {
      // typedef Container<TH1, 2> Base_t;
      /** Constructor */
      Chan() {}
      /** Constructor 
	  @param n Id of channel 
	  @param chip Back link to chip */
      Chan(unsigned int n, Chip& chip);
      /** @return Get name */
      const char* GetName() const;
      /** @return reference to mother board */
      const Chip& Mother() const { return *fChip; }
      /** @return chip number */ 
      unsigned int ChipNo() const { return fChip->Id(); }
      /** @return Board number */ 
      unsigned int BoardNo() const { return fChip->Mother().Id(); }
      /** @return Board number */ 
      unsigned int RcuNo() const { return fChip->Mother().Mother().Id(); }
      /** Fill an ADC value into the appropriate spectrum
	  @param sample Sample number
	  @param data ADC value */
      void Fill(short sample, unsigned int data);
      /** Get the spectrum of the specified sample
	  @param sample Sample to get spectrum for
	  @return The spectrum */
      TH1* GetSpectrum(short sample);
      /** Clear (delete) all contained objects */
      virtual void Clear();
      /** Reset contents */
      virtual void Reset();
      /** @return number of contained object (recursive) */
      virtual size_t Count() const;
      /** Write histograms to disk. */
      void WriteOut();
      /** Hide member function */
      TH1* GetOrAdd(unsigned int id);
    protected:
      /** Write out an element 
	  @param e Pointer to element to write out */
      virtual void WriteElem(Elem_t* e);
      /** Count sub-elements in this container.  This just returns 1. 
	  @return always 1 */
      virtual size_t CountElem(Elem_t*)  const { return 1; }
      /** Back link to chip */
      Chip* fChip;
      // ClassDef(Chan,1);
    };  
  }
}

#endif
//
// EOF
//
