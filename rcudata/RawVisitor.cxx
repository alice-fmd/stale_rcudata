//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    RawVisitor.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sat Jul  8 01:41:17 2006
    @brief   Implementation of RawVisitor
*/
#include "RawVisitor.h"
#include "DebugGuard.h"
#include <TTree.h>
#include <TFile.h>
#include <TClonesArray.h>
#include <iostream>
#include <rcudata/ProgressMeter.h>
#include <TParameter.h>

//____________________________________________________________________
RcuData::RawVisitor::RawVisitor(const char* oname, bool progress)
  : fFile(0), fTree(0), fMeter(0)
{
  DGUARD("ctor (%s)", oname);
  if (progress)               fMeter = new ProgressMeter(-1, 20);
  if (oname && oname[0] != '\0') {
    std::cout << "Writing data tree to " << oname << std::endl;
    fFile  = TFile::Open(oname, "RECREATE");
  }
  if (!fFile) { 
    std::cout << "No output produced by RcuData::RawVisitor" 
	      << std::endl;
    return;
  }
  // fFile->cd();
  fTree = new TTree("data", "Data");
  fTree->Branch("channel", &fArray);
}

//____________________________________________________________________
RcuData::RawVisitor::~RawVisitor()
{
  DGUARD("dtor");
  if (!fTree) return;
  TFile* file = fTree->GetCurrentFile();
  if (!file) {
    std::cerr << "Argh! No file associated with tree" << std::endl;
    return;
  }
  std::cout << "Writing output file " << file->GetName() 
	    << " ... " << std::flush;
  file->cd();
  file->Write();
  file->Close();
  std::cout << "done" << std::endl;
}

//____________________________________________________________________
bool
RcuData::RawVisitor::Visit(const ReadRaw::Event& e) 
{
  DGUARD("visiting event %p", &e);
  bool ret = ReadRaw::Visitor::Visit(e); 
  if (fMeter) fMeter->Step();
  if (!ret)   {
    std::cerr << "ReadRaw::Visitor::Visit failed" << std::endl;
    return ret;
  }
  if (fTree)  {
    // std::cout << "filling tree" << std::endl;
    fTree->Fill();
  }
  return ret;
}

//____________________________________________________________________
bool
RcuData::RawVisitor::Visit(const ReadRaw::Equipment& equipment)
{
  DGUARD("visiting equiment %p (0x%06x)", &equipment, equipment.Id());
  fDDL = equipment.Id();
  return ReadRaw::Visitor::Visit(equipment);
}

//____________________________________________________________________
bool
RcuData::RawVisitor::Visit(const uint32_t* data, 
			   const uint32_t  size) 
{
  DGUARD("visit payload @ %p of size %d", data, size);
  // Printer::Visit(data);
  // fArray->Clear();
  if (!AltroDecoder::DecodeEvent(data, size)) {
    std::cerr << "RcuData::RawVisitor::Visit(data): Failed at decode" 
	      << std::endl;
    return false;
  }
  return true;
}

//____________________________________________________________________
unsigned long
RcuData::RawVisitor::EstimatedNEvents() const
{
  DGUARD("from meter");
  return (fMeter ? fMeter->N() : -1);
}

//____________________________________________________________________
void 
RcuData::RawVisitor::SetEstimatedNEvents(uint32_t n)
{
  DGUARD("set meter");
  if (fMeter) fMeter->SetN(n);
}


//____________________________________________________________________
//
// EOF
//


  



