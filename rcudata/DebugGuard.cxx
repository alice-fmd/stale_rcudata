//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    rcuxx/DebugGuard.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 03:33:05 2006
    @brief   Implementation of DebugGuard
*/
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#ifndef RCUXX_DEBUGUARD_H
# include "DebugGuard.h"
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __SSTREAM__
# include <sstream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif
#ifndef __CSTDARG__
# include <cstdarg>
#endif

#ifndef _NDEBUG
//====================================================================
// bool RcuData::DebugGuard::fDebug = false;
int  RcuData::DebugGuard::fLevel = 0;
bool RcuData::DebugGuard::fDebug = false;

//____________________________________________________________________
RcuData::DebugGuard::DebugGuard(const char* func, const char* fmt, ...) 
{
  if (!fDebug) return;
  static char buf[1024];
  va_list ap;
  va_start(ap,fmt);
#ifdef WIN32
  vsprintf(buf, fmt, ap);
#else
  vsnprintf(buf, 1024, fmt, ap);
#endif
  va_end(ap);
  std::stringstream str;
  str << " <" << func << ">: " << buf;
  fMessage = str.str();
  std::cout << std::setw(fLevel+2) << "=>" << fMessage << std::endl;
  fLevel++;
}

//____________________________________________________________________
RcuData::DebugGuard::~DebugGuard()
{
  if (fMessage.empty()) return;
  fLevel--;
  std::cout << std::setw(fLevel+2) << "<=" << fMessage << std::endl;
}

//____________________________________________________________________
void
RcuData::DebugGuard::Message(const char* func, const char* fmt, ...) 
{
  if (!fDebug) return;
  static char buf[1024];
  va_list ap;
  va_start(ap,fmt);
#ifdef _WIN32
  vsprintf(buf, fmt, ap);
#else
  vsnprintf(buf, 1024, fmt, ap);
#endif
  va_end(ap);
  std::cout << std::setw(fLevel+2) << "==" << '<' << func 
	    << ">: " << buf << std::endl;
}
#endif

//
// EOF
//
