//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#include <rcudata/Acq.h>
#include <TTree.h>
#include <TFile.h>
#include <TClonesArray.h>
#include <TParameter.h>
#include <TError.h>
#include <rcudata/Channel.h>
#include <rcuxx/DebugGuard.h>
#include <rcuxx/rcu/RcuTRCFG1.h>
#include <rcuxx/rcu/RcuACTFEC.h>
#include <rcuxx/rcu/RcuACL.h>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <unistd.h>

//____________________________________________________________________
unsigned int
RcuData::Acq::WriteInfo()
{
  Rcuxx::DebugGuard g(fDebug, "Acq::WriteInfo");

  unsigned int ret = 0;
  typedef TParameter<int> Param_t;
  std::cout << "Writing Run information to file\n  "
	    << "Trigger(" 
	    << (fMode == kSoftwareTrigger ? "soft" : "hard")
	    << ") Run(" << fRun << ")" << std::endl;
  Param_t* trigger = new Param_t("trigger", (fMode == kSoftwareTrigger?0:1)); 
  trigger->Write();
  Param_t* run = new Param_t("runno", fRun);
  run->Write();

  TDirectory* rcu = fFile->mkdir("Rcu");
  rcu->cd();

  std::cout << "Writing RCU information to file\n  " << std::flush;
  Rcuxx::RcuTRCFG1* trcfg1 = fRcu.TRCFG1();
  if (trcfg1) {
    if ((ret = trcfg1->Update())) return ret;
    TDirectory* d = rcu->mkdir(trcfg1->Name().c_str());
    d->cd();
    
    Param_t* opt  = new Param_t("opt",trcfg1->IsOpt()); opt->Write();
    Param_t* pop  = new Param_t("pop",trcfg1->IsPop()); pop->Write();
    Param_t* nbuf = new Param_t("nbuf",
				(trcfg1->BMD()==Rcuxx::RcuTRCFG1::k4Buffers?
				 4:8)); nbuf->Write();
    Param_t* mode = new Param_t("mode",
				(trcfg1->Mode()==Rcuxx::RcuTRCFG1::kInternal?0:
				 trcfg1->Mode()==Rcuxx::RcuTRCFG1::kDerivedL2?
				 2:3)); mode->Write();
    Param_t* tw = new Param_t("tw",trcfg1->Twv()); tw->Write();
    if (fDebug) trcfg1->Print();
  }
  
  Rcuxx::RcuACTFEC* actfec = fRcu.ACTFEC();
  if (actfec) {
    if ((ret = actfec->Update())) return ret;
    TDirectory* d = rcu->mkdir(actfec->Name().c_str());
    d->cd();
    Param_t* value = new Param_t("value",actfec->Value()); value->Write();
    std::cout << "ACTFEC: (0x" << std::hex << actfec->Value() 
	      << std::dec << ") " << std::flush;
    usleep(fWait);

    if (fDebug) {
      Rcuxx::RcuACL* aclI = fRcu.ACL();
      for (size_t i = 0; i < 32; i++) {
	if (!actfec->IsOn(i)) continue;
	std::cout << "Board # " << i << std::endl;
	for (size_t j = 0; j < 8; j++) {
	  std::cout << "\tChip # " << i << ":";
	  for (size_t k = 0; k < 16; k++) 
	    std::cout << (aclI->CheckChannel(i, j, k) ? '-' : '+');
	  std::cout << std::endl;
	}
      }
    }
  }
  std::cout << std::endl;
  return ret;
}


//____________________________________________________________________
unsigned int
RcuData::Acq::Setup(int          nRun, 
			int          nevents, 
			Trigger_t    mode,
			unsigned int mask, 
			unsigned int addr)
{
  Rcuxx::DebugGuard g(fDebug, "Setting up for a run (%d,%d,%d,0x%x,0x%x)",
		      nRun, nevents, mode, mask, addr);

  unsigned int ret = 0;
  if ((ret = Rcuxx::Acq::Setup(nRun, nevents, mode, mask, addr))) return ret;

  if (fTree) {
    if (fTree->GetCurrentFile()) {
      fTree->GetCurrentFile()->Close();
      fFile = 0;
    }
    delete fTree;
    fTree = 0;
  }
  if (fOutName.empty()) fOutName = Form("%05d.root", fRun);
  Rcuxx::DebugGuard::Message(fDebug, "Opening file %s", fOutName.c_str());
  fFile = TFile::Open(fOutName.c_str(), "RECREATE");
  if (!fFile) {
    std::cerr << "Failed to open file " << fOutName << std::endl;
    return -kStopped;
  }
  fTree  = new TTree("data", "Data tree");
  // fArray = new TClonesArray("RcuData::Channel");
  fTree->Branch("channel", &fArray, 1024000);

  // ret = WriteInfo();
  return ret;
}

//____________________________________________________________________
unsigned int
RcuData::Acq::Run() 
{
  Rcuxx::DebugGuard g(fDebug, "Acq::Run()");
  if (fStop) return 1;

  unsigned int ret = 0;
  // Write information to the file 
  if ((ret = WriteInfo())) return ret;
  
  // Call base class member function 
  Rcuxx::DebugGuard::Message(fDebug, "Starting run");
  ret = Rcuxx::Acq::Run();
  fOutName.erase();
  if (ret) 
    std::cerr << "Error from Rcuxx::Acq::Run: " << ret << std::endl;
  if (fTree) {
    Rcuxx::DebugGuard::Message(fDebug, "Closing file %s", fFile->GetName());
    fTree->GetCurrentFile()->Write();
    fTree->GetCurrentFile()->Close();
    fFile = 0;
    fTree = 0;
  }
  return ret;
}

//____________________________________________________________________
int
RcuData::Acq::StartEvent(unsigned int evno) 
{
  Rcuxx::DebugGuard g(fDebug, "Acq::StartEvent(%d)", evno);
  Rcuxx::Acq::StartEvent(evno);
  fArray->Clear();
  return 0;
}

//____________________________________________________________________
int
RcuData::Acq::EndEvent(unsigned int evno) 
{
  Rcuxx::DebugGuard g(fDebug, "Acq::EndEvent(%d)", evno);
  fTree->Fill();
  return 0;
}

//____________________________________________________________________
int
RcuData::Acq::ProcessChannel(const DataVector_t& data, 
			     unsigned int size) 
{
  Rcuxx::DebugGuard g(fDebug, "Acq::ProcessChannel(%p,%d)", 
		      &(data[0]), size);

  if (fDebug) {
    Rcuxx::DebugGuard::Message(fDebug, "Got data of size %d", size);
#if 1
    for (size_t i = 0; i < size; i++) {
      printf("%4d 0x%04X 0x%04X 0x%04X 0x%04X\n", int(i), 
	     data[i*4+0], data[i*4+1], data[i*4+2], data[i*4+3]);
    }  
#endif
  }
  if (size <= 0) {
    Warning("ProcessChannel", "Size is weird %d", size);
    return 0;
  }
  size_t isize = 4 * size;
  DecodeChannel(data, isize);
#if 0
  Int_t n = fArray->GetEntries();
  Channel* channel = new ((*fArray)[n]) Channel(&(data[0]), 4 * size);
  Rcuxx::DebugGuard::Message(fDebug, "Board: 0x%x Chip: 0x%x Channel: 0x%x", 
			     channel->fBoard, channel->fChip, 
			     channel->fChannel);
  if (channel->fBoard > 0x1F) 
    Warning("ProcessChannel", "Bad board #: 0x%x", channel->fBoard);
  if (channel->fChip > 0x7) 
    Warning("ProcessChannel", "Bad chip #: 0x%x", channel->fChip);
  if (channel->fChannel > 0xf) 
    Warning("ProcessChannel", "Bad channel #: 0x%x", channel->fChannel);
  // PrintTrailer(&(data[(size - 2) * 4]));
#endif
  return 0;
}

//____________________________________________________________________
//
// EOF
// 
