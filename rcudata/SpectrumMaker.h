// -*- mode: C++ -*-
//____________________________________________________________________
//
// $Id: SpectrumMaker.h,v 1.9 2009-02-09 23:09:45 hehi Exp $
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    SpectrumMaker.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Base class for reading channel information.
*/
#ifndef RCUDATA_SPECTRUMMAKER_H
#define RCUDATA_SPECTRUMMAKER_H
#ifndef RCUDATA_READER
# include <rcudata/Reader.h>
#endif
#ifndef RCUDATA_SPECTRA
# include <rcudata/Spectra.h>
#endif
#ifndef RCUDATA_PROGRESSMETER
# include <rcudata/ProgressMeter.h>
#endif
#ifndef __CCTYPE__
# include <cctype>
#endif
class TFile;

namespace RcuData
{
  struct Channel;
  
  /** @struct SpectrumMaker 
      @brief Maker of spectra 
      @ingroup rcudata 
   */
  struct SpectrumMaker : public ChannelVisitor
  {
    /** Constructor */
    SpectrumMaker();
    /** Called when ever we get a new channel from reader.
	@param c      Channel object
	@param hasAll @c true if @a c has all information.  
	@return @c true, means continue with this channel */
    virtual bool GotChannel(Channel& c, bool hasAll);
    /** Called when ever we get a new ADC value for the current
	channel. 
	@param t   Time bin
	@param adc ADC value 
	@return  @c true, means continue with this channel */
    virtual bool GotData(uint32_t t, uint32_t adc);
    /** Start of job.
	@param src   Input source
	@param out   Output file
	@param n     Number of events to analyze
	@param skip  Number of events to skip. 
	@param tree  Whether to make an output tree 
	@param all   Analyse all events 
	@param wait  Wait for data */
    virtual void Exec(const char* src, 
		      const char* out, 
		      long n=-1, 
		      size_t skip=0, 
		      bool tree=true, 
		      bool all=true, 
		      bool wait=true);
    /** Start of job.
	@param src   Input source
	@param out   Output file
	@param n     Number of events to analyze
	@param skip  Number of events to skip. 
	@param tree  Also output tree  
	@param all   Read all events
	@param wait  Wait for data if @c true 
	@return  @c true on success, @c false otherwise */
    virtual bool Start(const char* src, 
		       const char* out, 
		       long n=-1, 
		       size_t skip=0, 
		       bool tree=true, 
		       bool all=true, 
		       bool wait=true);
    /** Run the event loop */ 
    virtual void Loop();
    /** Called at end of job. */
    virtual void End();
    /** Process on ADC spectra 
	@param spec     The spectra
	@param x        Bin center corresponding to this time bin.
	@param board    Board number
	@param chip     Chip number
	@param channel  Channel number
	@param t        Time bin. */
    virtual void ProcessSpectrum(TH1* spec, float x, 
				 unsigned ddl, 
				 unsigned board, 
				 unsigned chip, 
				 unsigned channel, 
				 unsigned t) {};
  protected:
    /** Find the cache variable for address passed. 
	@param iboard    Board number   
	@param ichip 	 Chip number    
	@param ichannel  Channel number 
	@return cache object  */
    Spectra::Chan* FindCurrent(uint32_t ircu, 
			       uint32_t iboard, 
			       uint32_t ichip, 
			       uint32_t ichannel);
    /** Calculate the bin center corresponding to passed address
	@param ddl      DDL number
	@param board    Board number
	@param chip     Chip number
	@param channel  Channel number 
	@param t        Time bin number
	@return Bin center corresponding to address */
    virtual float ToBin(unsigned ddl, 
			unsigned board, 
			unsigned chip, 
			unsigned channel, 
			unsigned t) const;
    /** Find min/max of bins, etc. */
    virtual void FindLimits();
    /** Loop over all spectra, and call ProcessSpectra for each*/
    virtual void LoopOverSpectra();
    /** Flush histograms to disk */
    virtual void WriteOut();
    
    /** Reader */
    Reader*        fReader;
    /** Output file */
    TFile*         fOutput;
    /** Cache */ 
    Spectra::Top     fTop;
    /** Current channel */
    Spectra::Chan*   fCurrent;
    /** Progress meter */
    ProgressMeter  fMeter;
    /** @{ 
	@name Minimum and maximum values */ 
    /** Least ddl number */
    uint32_t fMinDDL;
    /** Largest ddl number */
    uint32_t fMaxDDL;
    /** Distance in ddl numbers */
    uint32_t fDDDL;
    /** Least board number */
    uint32_t fMinBoard;
    /** Largest board number */
    uint32_t fMaxBoard;
    /** Distance in board numbers */
    uint32_t fDBoard;
    /** Least chip number */
    uint32_t fMinChip;
    /** Largest chip number */
    uint32_t fMaxChip;
    /** Distance in chip numbers */
    uint32_t fDChip;
    /** Least channel number */
    uint32_t fMinChan;
    /** Largest channel number */
    uint32_t fMaxChan;
    /** Distance in channel numbers */
    uint32_t fDChan;
    /** Least time bin number */
    uint32_t fMinT;
    /** Largest time bin number */
    uint32_t fMaxT;
    /** Distance in time bin numbers */
    uint32_t fDT;
    /** Least bin  */
    float fMinBin;
    /** Largest bin  */
    float fMaxBin;
    /** Number of bins */
    uint32_t fNBins;
    /** @} */
  };
}

#endif
//
// EOF
//

  
