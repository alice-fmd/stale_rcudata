//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUDATA_CHANNEL_H
# include <rcudata/Channel.h>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif
#ifndef ROOT_TGraph
# include <TGraph.h>
#endif
#ifndef ROOT_TH1
# include <TH1.h>
#endif
#ifndef ROOT_TTree
# include <TTree.h>
#endif
#ifndef ROOT_TClonesArray
# include <TClonesArray.h>
#endif

//____________________________________________________________________
typedef unsigned long long  w40_t;
static w40_t trailerMask = ((w40_t(0x2aaa) << 26) + (w40_t(0xa) << 12));

//____________________________________________________________________
ClassImp(RcuData::Channel);

//____________________________________________________________________
RcuData::Channel::Channel() 
  : fDDL(0), 
    fBoard(0),
    fChannel(0),
    fChip(0), 
    fLast(0),
    fM(1024),
    fN(0)
{
  for (Int_t i = 0; i < 1024; i++) fData[i] = 0;
}

//____________________________________________________________________
RcuData::Channel::Channel(const UInt_t ddl, const UInt_t* data, UInt_t size) 
  : fDDL(ddl),
    fBoard(0),
    fChannel(0),
    fChip(0), 
    fLast(0),
    fM(1024), 
    fN(0)
{
  w40_t trailer = ((w40_t(data[size - 1]) << 30)
		   + (w40_t(data[size - 2]) << 20) 
		   + (w40_t(data[size - 3]) << 10) 
		   +  data[size - 4]);
  if ((trailer & trailerMask) != trailerMask) {
    std::cerr << "Invalid trailer 0x" << std::hex << std::setw(10) 
	      << std::setfill('0') << trailer << " (0x" 
	      << std::setw(10) << trailerMask << ")" << std::dec 
	      << std::setfill(' ') << std::endl;
    return;
  }
  fChannel = trailer         & 0xf;
  fChip    = (trailer >> 4)  & 0x3;
  fBoard   = (trailer >> 7)  & 0x1f;
  fLast    = (trailer >> 16) & 0x3ff;
  UInt_t nfill = (fLast % 4 == 0 ? 0 : 4 - fLast %  4);
  Int_t i      = size - 5;
  if (i < 0) {
    std::cerr << "No data words!" << std::endl;
    return;
  }
  for (UInt_t j = 0; j < nfill; j++, i--) {
    if (data[i] != 0x2aa) {
      std::cerr << "Invalid fill " << j << "th word 0x" 
		<< std::hex << data[i] << std::dec << std::endl;
      return;
    }
  }
  while (i >= 0) {
    UInt_t l = data[i]; i--;
    UInt_t t = data[i]; i--;

    // std::cout << "New bunch ending at t=" << t << " of length " << l 
    //           << std::endl;
    for (Int_t j = 0; j < Int_t(l - 2); j++, i--) {
      if (i < 0 || i >= Int_t(size)) {
	std::cerr << "Index " << i << " larger than size " << size 
		  << " of data!" << std::endl;
	break;
      }
      if (t - j > fN) fN = t - j;
      if (t - j < fM) fM = t - j;
      if (data[i] > 1024 || data[i] < 0) 
	std::cerr << "Invalid data entry " << std::setw(3) << i 
		  << ": " << data[i] << std::endl;
      // std::cout << "Putting " << data[i] << " @ " << t - j << std::endl;
      fData[t - j] = data[i];
    }
  }
  for (i = fN; i < 1024; i++) fData[i] = 0;
  for (i = fM-1; i >= 0; i--) fData[i] = 0;
}

//____________________________________________________________________
RcuData::Channel::~Channel() 
{}

//____________________________________________________________________
void
RcuData::Channel::Print(Option_t* option) const
{
  std::cout << "Channel: " 
	    << std::setw(6) << fDDL << ":" 
	    << std::setw(2) << fBoard << "/" 
	    << std::setw(2) << fChip << "/" 
	    << std::setw(4) << fChannel << " (last " << fLast << "): "
	    << std::flush;
  for (Int_t i = fM; i <= fN; i++) {
    if (i % 6 == 0) std::cout << std::endl;
    std::cout << "  " << std::setw(3) << i 
	      << ": " << std::setw(4) << fData[i] << std::flush;
  }
  std::cout << std::endl;
}

//____________________________________________________________________
void
RcuData::Channel::PutInGraph(TGraph*& g) const
{
  if (!g) g = new TGraph;
  Int_t n = g->GetN();
  g->Set(n + fN + 1);
  for (Int_t i = n; i <= n + fN; i++)
    g->SetPoint(i, i, fData[i - n]);
}

//____________________________________________________________________
void
RcuData::Channel::PutInHist(TH1*& h, Int_t tmin, Int_t tmax, 
			    Double_t& off) const
{
  if (!h) h = new TH1D("adc", "ADC's", fN, -.5, fN+.5);
  for (Int_t i = 0; i <= fN; i++) {
    if ((tmin < 0 || i >= tmin) && (tmax < 0 || i <= tmax)) 
      h->SetBinContent(h->FindBin(off+i), fData[i]);
  }
  off += fN;
}

//____________________________________________________________________
Bool_t
RcuData::Channel::PlotSpectrum(TTree* tree, Int_t ddl, 
			       Int_t board, Int_t chip, 
			       Int_t channel, Int_t evno) 
{
  if (!tree) {
    std::cerr << "No tree" << std::endl;
    return kFALSE;
  }
  
  TString cut("channel.fData > 0");
  if (ddl     >= 0) cut.Append(Form(" && channel.fDDL == %d",     ddl));
  if (board   >= 0) cut.Append(Form(" && channel.fBoard == %d",   board));
  if (chip    >= 0) cut.Append(Form(" && channel.fChip == %d",    chip));
  if (channel >= 0) cut.Append(Form(" && channel.fChannel == %d", channel));
  if (evno    >= 0) cut.Append(Form(" && header.fEvent == %d",    evno));
  tree->Draw("channel.fData", cut.Data());
  return kTRUE;
}

//____________________________________________________________________
TGraph*
RcuData::Channel::PlotTimeSeries(TTree* tree, Int_t ddl,
				 Int_t board, Int_t chip, 
				 Int_t channel, Int_t evno, 
				 TClonesArray* channels) 
{
  Bool_t ourArray = (channels ? kFALSE : kTRUE);
  if (!tree) {
    std::cerr << "No tree" << std::endl;
    return 0;
  }
  if (!channels) channels = new TClonesArray("Channel");
  Header header;
  tree->SetBranchAddress("channel", &channels);
  tree->SetBranchAddress("header", &header);
  TGraph* hist = 0;
  Int_t n = tree->GetEntries();
  for (Int_t i = 0; i < n; i++) {
    tree->GetEntry(i);
    if (evno > -1 && header.fEventNo != evno) continue;
    Int_t m = channels->GetEntriesFast();
    for (Int_t j = 0; j < m; j++) {
      Channel* c = static_cast<Channel*>(channels->At(j));
      if ((ddl     < 0 || c->fDDL     == ddl) && 
	  (board   < 0 || c->fBoard   == board) && 
	  (chip    < 0 || c->fChip    == chip) && 
	  (channel < 0 || c->fChannel == channel)) {
	c->PutInGraph(hist);
      }
    } 
    if (evno > -1 && header.fEventNo == evno) break;
  }
  if (hist) hist->Draw("apl");
  if (ourArray) delete channels;
  return hist;
}

//____________________________________________________________________
TH1*
RcuData::Channel::PlotTimeHist(TTree* tree, Int_t ddl,
			       Int_t board, Int_t chip, 
			       Int_t chan, Int_t evno, Int_t tmin, 
			       Int_t tmax, TClonesArray* channels) 
{
  Bool_t ourArray = (channels ? kFALSE : kTRUE);
  if (!tree) {
    std::cerr << "No tree" << std::endl;
    return 0;
  }
  if (!channels) channels = new TClonesArray("Channel");
  Header header;
  tree->SetBranchAddress("channel", &channels);
  tree->SetBranchAddress("header", &header);
  TH1* hist = 0;
  Int_t minDDL   = Int_t(board < 0 ?tree->GetMinimum("channel.fDLL")   :ddl);
  Int_t maxDDL   = Int_t(board < 0 ?tree->GetMaximum("channel.fDDL")   :ddl);
  Int_t minBoard = Int_t(board < 0 ?tree->GetMinimum("channel.fBoard") :board);
  Int_t maxBoard = Int_t(board < 0 ?tree->GetMaximum("channel.fBoard") :board);
  Int_t minChip  = Int_t(chip < 0 ?tree->GetMinimum("channel.fChip")   : chip);
  Int_t maxChip  = Int_t(chip < 0 ?tree->GetMaximum("channel.fChip")   : chip);
  Int_t minChan  = Int_t(chan < 0 ?tree->GetMinimum("channel.fChannel"): chan);
  Int_t maxChan  = Int_t(chan < 0 ?tree->GetMaximum("channel.fChannel"): chan);
  Int_t minTime  = Int_t(tmin < 0 ?0 /*tree->GetMinimum("channel.fM")*/: tmin);
  Int_t maxTime  = Int_t(tmax < 0 ?tree->GetMaximum("channel.fN")      : tmax);
  Int_t minEvno  = (evno < 0 ? 0 : evno);
  Int_t maxEvno  = (evno < 0 ? tree->GetEntries() -1 : evno);
  Int_t dEvno    = (maxEvno  - minEvno + 1);
  Int_t dDDL     = (maxDDL   - minDDL + 1);
  Int_t dBoard   = (maxBoard - minBoard + 1);
  Int_t dChip    = (maxChip  - minChip + 1);
  Int_t dChan    = (maxChan  - minChan + 1);
  Int_t dTime    = (maxTime  - minTime + 1);
  hist = new TH1D("adcs", "ADC's", 
		  dEvno * dDDL * dBoard * dChip * dChan * dTime, 
		  minTime-.5, 
		  minTime + dEvno * dDDL * dBoard * dChip * dChan * dTime - .5);
  std::cout << "Evno:  "<<dEvno << " [" << minEvno  << "," << maxEvno  << "]\n"
	    << "DDL:   "<<dDDL  << " [" << minDDL   << "," << maxDDL   << "]\n"
	    << "Board: "<<dBoard<< " [" << minBoard << "," << maxBoard << "]\n"
	    << "Chip:  "<<dChip << " [" << minChip  << "," << maxChip  << "]\n"
	    << "Chan:  "<<dChan << " [" << minChan  << "," << maxChan  << "]\n"
	    << "Time:  "<<dTime << " [" << minTime  << "," << maxTime  << "]" 
	    << std::endl;

  Double_t off  = 0;
  for (Int_t i = minEvno; i <= maxEvno; i++) {
    tree->GetEntry(i);
    if (evno > -1 && header.fEventNo != evno) continue;
    Int_t m = channels->GetEntriesFast();
    for (Int_t j = 0; j < m; j++) {
      Channel* c = static_cast<Channel*>(channels->At(j));
      if ((ddl < 0   || c->fDDL     == ddl)   && 
	  (board < 0 || c->fBoard   == board) && 
	  (chip  < 0 || c->fChip    == chip)  && 
	  (chan  < 0 || c->fChannel == chan)) {
	off = ((i - minEvno) * dDDL * dBoard * dChip * dChan * dTime +
	       (j - minChan) * dTime);
#if 0
	std::cout << "Event # " << i << " Channel # " << j 
		  << " -> offset=(" << i << " - " << minEvno << ") * " 
		  << dChip << " * " << dChan << " * " << dTime << " + (" 
		  << j << " - " << minChan << ") * " << dTime << "=" 
		  << off << std::endl;
#endif
	c->PutInHist(hist, tmin, tmax, off);
      }
    } 
    if (evno > -1 && header.fEventNo == evno) break;
  }
  if (hist) hist->Draw();
  if (ourArray) delete channels;
  return hist;
}
  
//____________________________________________________________________
//
//EOF
//

