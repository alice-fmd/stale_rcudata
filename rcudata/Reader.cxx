//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Base class for reading channel information.
*/
#include "config.h"
#include "Reader.h"
#include "Channel.h"
#include "DebugGuard.h"
#include "Types.h"
#include <TChain.h>
#include <TClonesArray.h>
#include <TString.h>
#include <TSystem.h>
#include <TDirectory.h>
#include <iostream>
#include <cstdlib>

namespace 
{
  /** @struct TreeReader 
      @brief Read data from a TTree in a ROOT file. 
      This class should not be used directly.  Instead,
      RcuData::Reader::Create will make an object of this class if the
      name of a ROOT file is passed to that function. 
  */
  struct TreeReader : public RcuData::Reader
  {
    /** Constructor 
	@param n    Number of events to read
	@param skip Number of events to skip. */
    TreeReader(RcuData::ChannelVisitor& v, long n, size_t skip) 
      : RcuData::Reader(v, n, skip), 
	fChain("data"), 
	fArray(0)
    {
      DGUARD("ctor");
    }
    /** Destructor */
    virtual ~TreeReader() 
    { 
      DGUARD("dtor");
      if (fArray) delete fArray; 
    }
    
    /** Add an input file
	@param src Source file to add 
	@return @c true on success, @c false otherwise */
    bool SetInput(const char* src, bool, bool) 
    {
      DGUARD("%s", src);
      fChain.Reset();
      if (!fChain.Add(src) != 0) return false;
      TString base(gSystem->BaseName(src));
      TString dir(gSystem->DirName(src));
      int n = base.Length()-1;
      while (n > 0) {
	if (base[n] == '.') {
	  base.Remove(n, base.Length() - n);
	  break;
	}
	n--;
      }
      int i = 1;
      while (true) {
	TString fn(Form("%s_%d.root", base.Data(), i));
	char*   f  = gSystem->Which(dir.Data(), fn.Data(), kReadPermission);
	if (!f) break;
	std::cout << "Adding " << f << " to chain" << std::endl;
	fChain.Add(f);
	i++;
      }
      if (!fArray) 
	fArray = new TClonesArray("RcuData::Channel");
      fChain.SetBranchAddress("channel", &fArray);
      fIsEOD = false;
      return true;
    }
    
    /** Process one event.
	@return  @c true on success, @c false if there's no more data
	to be processed. */
    int GetNextEvent() 
    {
      DGUARD("get the next event");
      fIsEOD = false;
      // Check if we need to skip this event. 
      if (fCurrentEvent < fSkipEvents) {
	fCurrentEvent++;
	return kSkip;
      }
      
      // Check that we're not beyond what was requested 
      if (fMaxEvents > 0 && long(fCurrentEvent) >= fMaxEvents) {
	fIsEOD = true;
	return kMaxReached;
      }
      
      // Get the next entry. 
      if (fChain.GetEntry(fCurrentEvent) <= 0) {
	fIsEOD = true;
	return kEndOfData;
      }
      
      // Loop over the channels 
      DMESG("Loop over the channels");
      size_t nChannels = fArray->GetEntriesFast();
      for (size_t i = 0; i < nChannels; i++) {
	RcuData::Channel* channel = 
	  static_cast<RcuData::Channel*>(fArray->At(i));
	if (!channel) {
	  DMESG("No channel at %d", i);
	  continue;
	}

	// Call user code 
	if (!fProc.GotChannel(*channel, true)) continue;
	
	// Loop over data 
	size_t minT   = channel->MinT();
	size_t maxT   = channel->MaxT();
	for (size_t t = minT; t <= maxT; t++) 
	  if (!fProc.GotData(t, channel->fData[t])) break;
      }
      fProc.GotEvent(*fArray);
      
      // Increment event counter
      fCurrentEvent++;
      return kData;
    }
    /** @return the (estimated) number of events */
    long GetNumberOfEvents() const 
    {
      DGUARD("query tree");
      if (fMaxEvents < 0) return fChain.GetEntries();
      return fMaxEvents;
    }
  protected:
    TChain        fChain;
    TClonesArray* fArray;
  };
}

#ifdef HAVE_READRAW
# include <readraw/Reader.h>
# include "RawVisitor.h"

namespace 
{    
  using RcuData::uint32_t;
  /** @struct TreeReader 
      @brief Read data from a (set of) DATE raw files.
      This class should not be used directly.  Instead,
      RcuData::Reader::Create will make an object of this class if the
      name of a DATE raw file is passed to that function. 
  */
  struct RawReader : public RcuData::Reader, RcuData::RawVisitor
  {
    /** Constructor 
	@param output Output file, if any. 
	@param n      Number of events to read 
	@param skip   Number of events to skip */
    RawReader(RcuData::ChannelVisitor& v, 
	      const char* output, long n, size_t skip)
      : Reader(v, n, skip), 
	RawVisitor(output, false),
	fReader(0), 
	fMask(0),
	fLastEvent(0)
    {
      DGUARD("ctor");
    }
    
    /** Destructor */
    ~RawReader() 
    {
      DGUARD("dtor");
      if (fReader) delete fReader;
      if (fMask)   delete fMask;
    }
    /** Add an input source. Can only be called once. 
	@param src Source to add.  This may terminate the application
	if called more than once. 
	@return @c true on success, @c false otherwise.  */
    bool SetInput(const char* src, bool all, bool block) 
    {
      DGUARD("%s", src);
      if (fReader) {
	std::cerr << "Cannot add more input to a RawReader" << std::endl;
	exit(1);
      }
      fReader = ReadRaw::Reader::Create(src, true);
      if (!fReader) { 
	std::cerr << "No raw reader created for " << src << std::endl;
	return false;
      }
      fMask = new ReadRaw::Mask;
      fMask->AddToMask(ReadRaw::Mask::kPhysicsEvent, 
		       (all ? ReadRaw::Mask::kAll : ReadRaw::Mask::kSome));
      fReader->SetMask(*fMask); 
      fReader->SetWait(block);
      fIsEOD = false;
      return true;
    }
    
    /** Get the next event from the input.  
	@return @c true on succces, false otherwise.  */
    int GetNextEvent() 
    {
      DGUARD("Next event");
      fIsEOD = false;
      if (!fReader) {
	std::cerr << "No input set" << std::endl;
	fIsEOD = true;
	return kEndOfData;
      }
      // Check that we're not at the end. 
      if (fReader->IsEOD()) {
	// std::cout << "At EOD" << std::endl;
	fIsEOD = true;
	return kEndOfData;
      }
      
      // Check that we haven't passed the requested number of events. 
      if (fMaxEvents > 0 && int(fCurrentEvent) >= fMaxEvents) {
	fIsEOD = true;	
	// std::cout << "At MAX" << std::endl;
	return kMaxReached;
      }
      
      // Read in the next event 
      bool ret = fReader->GetNextEvent(fEvent);
      if (!ret) return kNoData;
      
      // Check whether we should skip this event.
      if (fCurrentEvent < fSkipEvents) {
	// std::cout << "Skipi" << std::endl;
	fCurrentEvent++;
	return kSkip;
      }
      
      // Get the event number 
      fLastEvent = fEvent.Head().Id().NumberInRun();
      
      // Now visit the event. 
      Visit(fEvent);
      
      // Deal with all channels 
      DMESG("Processing the event using a %s", typeid(fProc).name());
      fProc.GotEvent(*fArray);
      
      // Increment the event counter and return 
      fCurrentEvent++;
      return kData;
    }

    /** @return the (estimated) number of events */
    long GetNumberOfEvents() const 
    {
      DGUARD("Deduce from read data");
      if (fMaxEvents < 0 && fReader) return fReader->Events();
      return fMaxEvents;
    }
    
    /** Get the last read event number */ 
    long GetEventNumber() const 
    { 
      DGUARD("The evnet number is %d", fLastEvent);
      return fLastEvent; 
    }
  protected:  
    /** Called when we got a new channel.
	@param board   FEC address
	@param chip    ALTRO adddress
	@param channel ALTRO channel number
	@param last    Number of words in data */
    void GotChannel(uint32_t ddl, 
		    uint32_t board, 
		    uint32_t chip, 
		    uint32_t channel, 
		    uint32_t last) 
    {
      DGUARD("Got channel 0x%04x:0x%02x/0x%x/0x02x (size %d)",
	     ddl,board,chip,channel);
      RcuData::RawVisitor::GotChannel(ddl,board, chip, channel, last);
      fSkipChannel = false;
      if (!fCurrent) { 
	std::cerr << "No current channel!" << std::endl;
	return;
      }
      fSkipChannel = !fProc.GotChannel(*fCurrent, false);
    }
    /** Called when we got data for a time bin for the current
	channel.
	@param t   Timebin.  Note, @c t==14 corresponds to the time
	of the level 1 trigger
	@param adc ADC value at @a t. */
    void GotData(uint32_t t, uint32_t adc) 
    {
      DGUARD("Got data %d at time %d",t, adc);
      RcuData::RawVisitor::GotData(t, adc);
      if (fSkipChannel) return;
      if (!fCurrent) {
	std::cout << "No current channel " << std::endl;
	return;
      }
      fSkipChannel = !fProc.GotData(t, adc);
    };
    /** The raw reader */ 
    ReadRaw::Reader* fReader;
    /** The mask for the raw reader */ 
    ReadRaw::Mask*   fMask;
    /** Event to read into */ 
    ReadRaw::Event   fEvent;
    /** whether we should skip the rest of this channel */ 
    bool fSkipChannel;
    /** Last read event number */
    long fLastEvent;
  };
} 
#endif
  
      
//____________________________________________________________________
RcuData::Reader*
RcuData::Reader::Create(ChannelVisitor& v, 
			const char*     src, 
			long            n, 
			unsigned int    skip, 
			bool            tree, 
			bool            all,
			bool            block)
{
  DGUARD("making a reader from %p,%s,%d,%d", &v, src, n, skip);
  TString f(src);
  if (f.IsNull()) { 
    std::cerr << "Null input specified" << std::endl;
    return 0;
  }
  Reader* r = 0;
  
  if (f.EndsWith(".root")) r = new TreeReader(v, n, skip);
  else {
#ifdef HAVE_READRAW
    TDirectory* savdir = gDirectory;
    TString o;
    if (tree) {
      o = Form("data_%s",gSystem->BaseName(src));
      int n = o.Length()-1;
      while (n > 0) {
	if (o[n] == '.') {
	  o.Remove(n, o.Length() - n);
	  if (!o.EndsWith(".000")) break;
	}
	n--;
      }
      o.Append(".root");
    }
    r = new RawReader(v, o.Data(), n, skip);
    if (savdir) savdir->cd();
#else 
    std::cerr << "Invalid type of input '" << src << "' - giving up" 
	      << std::endl;
#endif
  }
  if (!r) return 0;
  if (!r->SetInput(src, all, block)) {
    std::cerr << "Failed to set input to " << src << std::endl;
    delete r;
    r = 0;
  }
  return r;
}
//____________________________________________________________________
//
// EOF
//

  
