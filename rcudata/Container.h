// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Container.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:08 2006
    @brief   Container of various data types 
    @ingroup rcudata_data        
*/
#ifndef RCUDATA_CONTAINER
#define RCUDATA_CONTAINER
#ifndef __MAP__
# include <map>
#endif
#ifndef __SSTREAM__
# include <sstream>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif
#ifndef ROOT_TObject
# include <TObject.h>
#endif
#ifndef ROOT_TDirectory
# include <TDirectory.h>
#endif
#ifndef ROOT_TH1
# include <TH1.h>
#endif
#ifndef ROOT_TBrowser
# include <TBrowser.h>
#endif

namespace RcuData 
{
  //________________________________________________________________
  /** @class Container
      @brief Class template for a container of cache information. 
   */
  template <typename T> 
  struct Container : public TObject
  {
    /** Contained object type */
    typedef T Elem_t;
    /** Map of contained object */ 
    typedef std::map<unsigned int, Elem_t*> Cont_t;
    /** iterator over contained objects */ 
    typedef typename Cont_t::iterator Iter_t;
    /** Constant iterator over contained objects */ 
    typedef typename Cont_t::const_iterator ConstIter_t;
      
      
    /** Constructor 
	@param id Identifier  */
    Container(unsigned int id=0) 
      : fId(id), 
	fSummary(0)
    {}
    /** Destructor */ 
    virtual ~Container() { Clear(); }
    /** Clear (delete) all contained objects */
    virtual void Clear()
    {
      for (Iter_t i = Begin(); i != End(); ++i) {
	delete i->second;
	i->second = 0;
      }
      fCont.clear();
    }
    /** Reset contents */
    virtual void Reset() 
    {
      for (Iter_t i = Begin(); i != End(); ++i) i->second->Reset();
      if (fSummary) fSummary->Reset();
    }
    /** @return the identifier */ 
    unsigned int Id() const { return fId; }

    /** Return object at @a id, or make it if it doesn't exist 
	@param id Object to get 
	@return Pointer to object at @a id (possibly newly
	allocated) */ 
    virtual Elem_t* GetOrAdd(unsigned int id) = 0;
    /** Return object at @a id if it exists. 
	@param id Object to get 
	@return object at @a id, or 0 if it doesn't exist */
    Elem_t* Get(unsigned int id) 
    {
      return fCont[id];
    }

    /** @return summary histogram */
    TH1* GetSummary() const { return fSummary; }
      
    /** @return iterator pointing to beginning of container */
    Iter_t Begin() { return fCont.begin(); }
    /** @return iterator pointing to beginning of container */
    ConstIter_t Begin() const { return fCont.begin(); }
    /** @return iterator pointing to end of container */
    Iter_t End() { return fCont.end(); }
    /** @return iterator pointing to end of container */
    ConstIter_t End() const { return fCont.end(); }
    /** @return number of contained object (recursive) */
    virtual size_t Count() const
    {
      size_t ret = 0;
      for (ConstIter_t i = Begin(); i != End(); ++i) 
	ret += CountElem(i->second);
      return ret;
    }
    /** @return The name of this object */ 
    virtual const char* GetName() const = 0;
#if 0
    /** @return always @c true */ 
    virtual Bool_t IsFolder() const { return kFALSE; }
    /** Browse this object 
	@param b Browser browsing this object */
    virtual void Browse(TBrowser* b) 
    {
      if (fSummary) b->Add(fSummary);
      for (Iter_t i = Begin(); i != End(); ++i) 
	if (i->second) b->Add(i->second);
    }
#endif 
    /** Write contained objects to disk. */
    virtual void WriteOut() 
    {
      std::string name(GetName());
      TDirectory* savDir = gDirectory;
      if (savDir) {
	TDirectory* dir    = 0;
	if (!(dir = gDirectory->GetDirectory(name.c_str())))
	  dir = gDirectory->mkdir(name.c_str());
	dir->cd();
      }
      for (ConstIter_t i = Begin(); i != End(); ++i) WriteElem(i->second);
      if (fSummary) fSummary->Write();
      if (savDir) savDir->cd();
    }
  protected:
    /** Make summary histogram 
	@param nbins Number of bins 
	@param low   Low edge 
	@param high  High edge */ 
    void MakeSummary(unsigned nbins, Float_t low, Float_t high) 
    {
      std::string name(GetName());
      fSummary = new TH1I(name.c_str(), name.c_str(), 1024, -.5, 1023.5);
      fSummary->SetXTitle("ADC counts");
      fSummary->SetYTitle("Events");
      fSummary->SetFillStyle(3001);
      fSummary->SetDirectory(0);
    }
    /** Write out an element 
	@param e Pointer to element to write out */
    virtual void WriteElem(Elem_t* e) 
    {
      // if (!e) return;
      // e->WriteOut();
    }
    /** Count sub-elements in this container.  This is used for the
	final progress bar. 
	@param e element to do counting for 
	@return Total count of elements in @a e and it's daughters */
    virtual size_t CountElem(Elem_t* e) const = 0;
    /** Container of objects */ 
    Cont_t fCont;
    /** Identifer */
    unsigned int fId;
    /** Summary histogram */
    TH1* fSummary;
  };
}

#endif
//
// EOF
//
