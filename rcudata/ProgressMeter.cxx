// -*- mode: c++ -*-  
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    ProgressMeter.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Feb 13 01:04:26 2006
    @brief   ProgressMeter implementation
*/
#ifndef RCUDATA_PROGRESSMETER_H
# include <rcudata/ProgressMeter.h>
#endif 
#ifndef __CTIME__
# include <ctime>
#endif
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __IOMANIP__
# include <iomanip>
#endif
#ifndef __CERRNO__
# include <cerrno>
#endif

//____________________________________________________________________
RcuData::ProgressMeter::ProgressMeter(int n, int freq)
  : fStart(0), 
    fNow(0), 
    fN(n), 
    fCurrent(0), 
    fFreq(freq), 
    fThrobber('-'), 
    fPre("Event #"), 
    fDump(false)
{
  Reset(n);
  fFreq = freq;
}

//____________________________________________________________________
void
RcuData::ProgressMeter::Reset(int n, const char* pre, int freq) 
{ 
  fN        = n; 
  fCurrent  = 0; 
  fThrobber = '-';  
  fPre      = std::string(pre); 
  fFreq     = (freq == 0 ? fFreq : freq);
  // Check whether the output is redirected - this is some magic here,
  // that may not work on all platforms! 
  fDump = true;
  std::cout.seekp(0, std::ios_base::cur);
  if (std::cout.fail()) {
    std::cout.clear();
    if (errno == ESPIPE) fDump = false;
  }
}

//____________________________________________________________________
void
RcuData::ProgressMeter::SetDump(bool dump) 
{ 
  // Force a reset!
  Reset(fN, fPre.c_str(), fFreq);
  // Override whatever the automatic check found. 
  fDump = dump; 
}

//____________________________________________________________________
void
RcuData::ProgressMeter::Step()
{
  if (fCurrent == 0) fStart = time(NULL);
  fCurrent++;
  if (!(fCurrent % fFreq == 0) && int(fCurrent) != fN) return;
  fNow = time(NULL);
  
  PrintPrefix();
  PrintBar();
  PrintPercentage();
  PrintEta();
  PrintThrobber();
  std::cout << (fDump ? "\n" : "\r") << std::flush;
}

//____________________________________________________________________
size_t
RcuData::ProgressMeter::PrintPrefix()
{
  // Event number output - 17 characters 
  std::cout << fPre << " " << std::setw(8) << fCurrent << " ";
  return fPre.size() + 1 + 8 + 1;
}

//____________________________________________________________________
size_t
RcuData::ProgressMeter::PrintBar()
{
  if (fDump) return 0;
  // Bar - (75-10-pre_size-5-14-1) = 45 characters
  int       c = int(fCurrent);
  int       m = (fN < 0 ? 5                : 0) + 45 - fPre.size();
  char      b = (fN < 0 ? ' '              : '=');
  char      a = (fN < 0 ? '='              : (c >= fN ? '=' : '>'));
  int       n = (fN < 0 ? fCurrent % m + 1 : int(float(m * fCurrent)/fN)+1);
  int       p = (fN < 0 ? m - n + 2        : (c >= fN ? 1 : (m-n+2)));
  if (n > 45 || p > 45) { 
    std::cerr << "n=" << n << " p=" << p << std::endl;
    std::cout << std::setfill('+') << std::setw(45) << ' ';
    return 45;
  }
  std::cout << std::setfill(b)   << std::setw(n) << a;
  std::cout << std::setfill(' ') << std::setw(p) << ' ';
  return 45;
}


//____________________________________________________________________
size_t
RcuData::ProgressMeter::PrintPercentage()
{
  if (fN < 0) return 0;
  std::cout << std::setw(3) << (fCurrent * 100) / fN << "% ";
  return 5;
}

 
//____________________________________________________________________
size_t
RcuData::ProgressMeter::PrintEta()
{
  // Time or ETA - 14 characters
  if (fCurrent == 1) { 
    std::cout << "ETA   --:--:-- ";
    return 14;
  }

  bool   done = fN <= int(fCurrent);
  bool   infi = fN < 0 || done;
  double elab = difftime(fNow,fStart);
  double full = elab / fCurrent * fN + .5;
  int    rem  = int(infi ? elab : full-elab);
  int    sec  = rem % 60;
  int    min  = (rem / 60) % 60;
  int    hou  = (rem / 60 / 60) % 24;
  std::cout << (infi ? "Elap " : (done ? "Done " : "ETA~ "))
	    << std::setw(3) << hou << ":"
	    << std::setfill('0')
	    << std::setw(2) << min << ":"
	    << std::setw(2) << sec << " ";
  std::cout << std::setfill(' ');
  return 14;
}

//____________________________________________________________________
size_t
RcuData::ProgressMeter::PrintThrobber()
{
  if (fDump) return 0;
  // Throbber - 1 character 
  switch (fThrobber) {
  case '*':  fThrobber = '-';  break;
  case '-':  fThrobber = '\\'; break;
  case '\\': fThrobber = '|';  break;
  case '|':  fThrobber = '/';  break;
  case '/':  fThrobber = '-';  break;
  }
  if (fN > 0 && int(fCurrent) >= fN) fThrobber = '*';
  std::cout << fThrobber;
  return 1;
}

//____________________________________________________________________
//
// EOF
//

