//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    SpectrumMaker.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Base class for reading channel information.
*/
#include "config.h"
#include "SpectrumMaker.h"
#include "Channel.h"
#include "DebugGuard.h"
#include <TFile.h>
#include <TClonesArray.h>
#include <TString.h>
#include <TSystem.h>
#include <iostream>
//____________________________________________________________________
RcuData::SpectrumMaker::SpectrumMaker()
  : fReader(0), 
    fOutput(0), 
    fCurrent(0), 
    fMeter(-1, 10),
    fMinDDL(0xFFFF), fMaxDDL(0),   fDDDL(0),
    fMinBoard(64),   fMaxBoard(0), fDBoard(0),
    fMinChip(16),    fMaxChip(0),  fDChip(0),
    fMinChan(32),    fMaxChan(0),  fDChan(0),
    fMinT(2048),     fMaxT(0),     fDT(0), 
    fMinBin(1e8),    fMaxBin(0),   fNBins(0)
{
  DGUARD("ctor");
}
  
//____________________________________________________________________
void
RcuData::SpectrumMaker::Exec(const char* src, const char* out, long n, 
			     size_t skip, bool tree, bool all, bool wait)
{
  DGUARD("%s,%s,%d,%d", src, out, n, skip);
  if (!Start(src, out, n, skip, tree, all, wait)) { 
    std::cout << "Start failed!" << std::endl;
    return;
  }
  std::cout << "Starting the loop" << std::endl;
  Loop();
  std::cout << "At end of loop" << std::endl;
  End();
}

//____________________________________________________________________
bool
RcuData::SpectrumMaker::Start(const char* src, const char* out, long n, 
			      size_t skip, bool tree, bool all, bool wait)
{
  DGUARD("%s,%s,%d,%d", src, out, n, skip);
  // Close old reader if any
  if (fReader) {
    delete fReader;
    fReader = 0;
  }

  // Make new reader 
  fReader = Reader::Create(*this, src, n, skip, tree, all, wait);
  if (!fReader) {
    std::cerr << "SpectrumMaker: No reader made for input " 
	      << src << std::endl;
    return false;
  }

  // Open output if any
  if (out && out[0] != '\0') {
    fOutput = TFile::Open(out, "RECREATE");
    if (!fOutput) 
      std::cerr << "Failed to make output file " << out << std::endl;
  }
  
  // Reset Progress meter.
  fMeter.Reset(-1, "Event #", 10);
  return true;
}

//____________________________________________________________________
void
RcuData::SpectrumMaker::Loop()
{
  DGUARD("read the data in loop");
  if (!fReader) {
    std::cerr << "No reader definend " << std::endl;
    return;
  }
  long ev = 0;
  
  while (true) {
    int  ret  = fReader->GetNextEvent();
    bool stop = false;
    switch (ret) {
    case Reader::kData:       
      // std::cout << "Data event" << std::endl;
      break;
    case Reader::kSkip:   
      // std::cout << "Skip - " << std::flush;
    case Reader::kNoData:     
      // std::cout << "No data" << std::endl;
      continue; break;
    case Reader::kEndOfData: 
      // std::cout << "EOD - " << std::flush;
    case Reader::kMaxReached:
      // std::cout << "MAX - " << std::flush;
    case Reader::kError:      
      // std::cout << "Error" << std::endl;
      stop = true; break;
    }
    ev++;
    fCurrent = 0;
    fMeter.Step();
    if (fReader->GetNumberOfEvents() != long(fMeter.N()))
      fMeter.SetN(fReader->GetNumberOfEvents());
    if (stop) break;
  }
  fMeter.SetFreq(1);
  for (;ev < long(fMeter.N()); ev++) fMeter.Step();
}

//____________________________________________________________________
void
RcuData::SpectrumMaker::End()
{
  DGUARD("done processing");
  FindLimits();
  LoopOverSpectra();
  WriteOut();
}

  
//____________________________________________________________________
bool
RcuData::SpectrumMaker::GotChannel(Channel& c, bool)
{
  DGUARD("channel @ %p", &c);
  fCurrent = FindCurrent(c.DDL(), c.Board(), c.Chip(), c.ChanNo());
  return true;
}

//____________________________________________________________________
RcuData::Spectra::Chan*
RcuData::SpectrumMaker::FindCurrent(uint32_t iddl, 
				    uint32_t iboard, 
				    uint32_t ichip, 
				    uint32_t ichannel)
{
  DGUARD("Looking for 0x%0x4/0x%02x/0x%x/0x%02x", 
	 iddl, iboard, ichip, ichannel);
  Spectra::Rcu*   rcu   = fTop.GetOrAdd(iddl);
  Spectra::Board* board = rcu->GetOrAdd(iboard);
  Spectra::Chip*  chip  = board->GetOrAdd(ichip);
  Spectra::Chan*  chan  = chip->GetOrAdd(ichannel);

  fMaxBoard = std::max(iboard,    fMaxBoard);
  fMinBoard = std::min(iboard,    fMinBoard);
  fMaxChip  = std::max(ichip,     fMaxChip);
  fMinChip  = std::min(ichip,     fMinChip);
  fMaxChan  = std::max(ichannel,  fMaxChan);
  fMinChan  = std::min(ichannel,  fMinChan);
  return chan;
}

//____________________________________________________________________
bool
RcuData::SpectrumMaker::GotData(uint32_t t, uint32_t adc) 
{
  DGUARD("data @ %4d is 0x%03x", t, adc);
  if (!fCurrent) return false;
  fTop.Fill(fCurrent->RcuNo(), fCurrent->BoardNo(), 
	    fCurrent->ChipNo(), fCurrent->Id(), t, adc);
  fMaxT  = std::max(t, fMaxT);
  fMinT  = std::min(t, fMinT);
  return true;
}

//____________________________________________________________________
float
RcuData::SpectrumMaker::ToBin(unsigned ddl, 
			      unsigned board, 
			      unsigned chip, 
			      unsigned chan, 
			      unsigned t) const
{
  DGUARD("Looking for bin of 0x%04x: 0x%02x/0x%x/0x%02x @ %4d", 
	 ddl, board, chip, chan, t);
  return (((ddl * fDBoard + board) * fDChip + chip) * fDChan + chan) * fDT + t;
}


//____________________________________________________________________
void
RcuData::SpectrumMaker::FindLimits()
{
  DGUARD("check axis range");
  fDT      = fMaxT     - fMinT     - 1;
  fDChan   = fMaxChan  - fMinChan  - 1;
  fDChip   = fMaxChip  - fMinChip  - 1;
  fDBoard  = fMaxBoard - fMinBoard - 1;
  fDDDL    = fMaxDDL   - fMinDDL   - 1;
  
  if      (fDChan > 10) fDChan = 20;
  else if (fDChan >  5) fDChan = 10;
  else                  fDChan =  5;
  
  if      (fDT > 1000)  fDT    = 1500;
  else if (fDT >  500)  fDT    = 1000;
  else if (fDT >  200)  fDT    =  500;
  else if (fDT >  100)  fDT    =  200;
  else if (fDT >   50)  fDT    =  100;
  else                  fDT    =   50;
  
  fMinBin = ToBin(fMinDDL, fMinBoard, fMinChip, fMinChan, fMinT);
  fMaxBin = ToBin(fMinDDL   + fDDDL   - 1, 
		  fMinBoard + fDBoard - 1, 
		  fMinChip  + fDChip  - 1, 
		  fMinChan  + fDChan  - 1,
		  fMinT     + fDT     - 1);
  Float_t dBin = (ToBin(fMinDDL, fMinBoard, fMinChip, fMinChan, fMinT+1) 
		  - fMinBin);
  fMinBin -= dBin / 2;
  fMaxBin += dBin / 2;
  fNBins  =  unsigned((fMaxBin - fMinBin) / dBin);
}


//____________________________________________________________________
void
RcuData::SpectrumMaker::LoopOverSpectra()
{
  DGUARD("Now check the data collected");
  std::cout << "\nProcessing spectra ... " << std::endl;
  typedef Spectra::Top::Iter_t   rcuIter;
  typedef Spectra::Rcu::Iter_t   boardIter;
  typedef Spectra::Board::Iter_t chipIter;
  typedef Spectra::Chip::Iter_t  channelIter;
  typedef Spectra::Chan::Iter_t  histIter;
  
  for (rcuIter ir = fTop.Begin(); ir != fTop.End(); ++ir) {
    Spectra::Rcu* rcu = ir->second;
    for (boardIter ib = rcu->Begin(); ib != rcu->End(); ++ib) {
      Spectra::Board* board = ib->second;
      for (chipIter ia = board->Begin(); ia != board->End(); ++ia){
	Spectra::Chip* chip = ia->second;
	for (channelIter ic = chip->Begin(); ic != chip->End(); ++ic){
	  Spectra::Chan* chan = ic->second;
	  // size_t n = chan->fStrips.size() - 4;
	  size_t n = chan->Count();
	  std::string s(Form("Rcu %d, Board %2d, Chip %d, Channel %2d",
			     rcu->Id(), board->Id(), chip->Id(), chan->Id()));
	  fMeter.Reset(n, s.c_str(), 1);
	  for (histIter ih = chan->Begin();ih != chan->End();++ih){
	    fMeter.Step();
	    TH1* spectrum = ih->second;
	    Float_t x = ToBin(rcu->Id(), board->Id(), chip->Id(), 
			      chan->Id(), ih->first);
	    ProcessSpectrum(spectrum, x, rcu->Id(), board->Id(), 
			    chip->Id(), chan->Id(), ih->first);
	  }
	  std::cout <<  std::endl;
	} // for (Chip::ChanMap_t ...
      }  // for (Board::ChipMap_t ...
    } // for (BoardMap_t ...
  }
}

//____________________________________________________________________
void
RcuData::SpectrumMaker::WriteOut()
{
  DGUARD("write output");
  if (fReader) {
    delete fReader;
    fReader = 0;
  }
  
  if (fOutput) {
    fOutput->cd();
    fTop.WriteOut();
    std::cout << "Flushing to disk, please wait ... " << std::flush;
    fOutput->Write();
    fOutput->Close();
    fOutput = 0;
    std::cout << "done" << std::endl;
  }
}


//____________________________________________________________________
//
// EOF
//

  
