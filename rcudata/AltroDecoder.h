// -*- mode: C++ -*-
//____________________________________________________________________
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jun 26 20:10:43 2006
    @brief   Declaration of decoder or ALTRO data 
*/
#ifndef RCUDATA_ALTRODECODER_H
#define RCUDATA_ALTRODECODER_H
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef RCUDATA_TYPES_H
# include <rcudata/Types.h>
#endif
class TClonesArray;

namespace RcuData
{
  /** Forward declaration */
  class Channel;
  
  /** @struct AltroDecoder rcudata/AltroDecoder.h <rcudata/AltroDecoder.h>
      @brief Decode ALTRO data 
   */
  struct AltroDecoder 
  {
    /** a 10bit word */ 
    typedef uint10_t Word10_t;
    /** a 32bit word */ 
    typedef uint32_t Word32_t;
    /** Type of vector of 10 bit words */
    typedef std::vector<Word10_t>  Word10Vector_t;
    /** Type of vector of 32 bit words */
    typedef std::vector<Word32_t> Word32Vector_t;
    /** Error codes */ 
    enum {
      kEOD  = 1,
      kRead, 
      kBadValue, 
      kBadTrailer, 
      kBadFill,
      kTooLong
    };
    
    /** construcutor */
    AltroDecoder();
    /** Destructor */
    virtual ~AltroDecoder() {}

    /** Clear internal array */
    virtual void Clear();
    /** Decode an event
	@param data Array of 32bit words 
	@param size Number of 32bit words in @a data
	@return @c true on success, @c false otherwise */
    virtual bool DecodeEvent(const Word32_t* data, 
			     const Word32_t  size);
    /** Decode a channel 
	@param data Array of 10bit words 
	@param size Number of words in this channel.  On return, this
	should be 0.  
	@return @c true on success, @c false otherwise */
    virtual int DecodeChannel(const Word10Vector_t& data, 
			      volatile size_t& size);
    /** Decode a channel 
	@return @c true on success, @c false otherwise */
    virtual int DecodeChannel();
    /** Decode the trailer of a channel 
	@param addr    On return, the full channel address
	@param last    Index of last data word in @a data 
	@return @c true if the trailer and fill words are valid,
	otherwise @c false */
    virtual int DecodeTrailer(uint32_t& addr, uint32_t& last);
    /** Decode the full channel address given in @a addr into it's
	components. 
	@param addr    Full channel address 
	@param board   On return, the board number 
	@param chip    On return, the chip number 
	@param channel On return, the channel number */
    void DecodeAddress(const uint32_t addr, uint32_t& board, 
		       uint32_t& chip, uint32_t& channel) const; 

    /** Get the array */
    TClonesArray* Array() { return fArray; }
  protected:
    /** Type of 40 bit words */
    typedef uint40_t w40_t;
    /** Trailer mask */ 
    static const w40_t fgkTrailerMask;
    /** Check if the 40 bit word @a w matches a trailer 
	@param w 40bit word to test
	@return  @c true if @a w matches a trailer */
    bool IsTrailer(const w40_t& w) const 
    { 
      return (w & fgkTrailerMask) == fgkTrailerMask; 
    }

    /** Called when we got a new channel. 
	@param board   FEC address
	@param chip    ALTRO adddress
	@param channel ALTRO channel number
	@param last    Number of words in data */
    virtual void GotChannel(uint32_t ddl,
			    uint32_t board, 
			    uint32_t chip, 
			    uint32_t channel, 
			    uint32_t last);
    
    /** Called when we got data for a time bin for the current
	channel. 
	@param t   Timebin.  Note, @c t==15 corresponds to the time
	of the level 1 trigger 
	@param adc ADC value at @a t. */
    virtual void GotData(uint32_t t, uint32_t adc);

    /** Read one 10bit word from input
	@return -1 if case of no more data, otherwise, the 10bit word
	*/
    int GetNext10bitWord();

    /** Decode the error. 
	@param err Error code */
    virtual void DecodeError(int err);
    
    /** Array of clones of data */ 
    TClonesArray*  fArray;
    /** Current DDL */
    uint32_t fDDL;
    /** Current channel, if any */ 
    Channel* fCurrent;
    /** Pointer to current position in input */
    uint8_t* fCur;
    /** Pointer to head of input */ 
    uint8_t* fStart;
    /** Number of 10 bit words in 40 bit buffer */
    size_t fN;
    /** Pointer to an array of decoded 10bit words */ 
    const Word10Vector_t* fDecoded;
    /** Size of decoded data vector */
    size_t fDecodedCur;
    /** Times EOD was seen */
    size_t fNEOD;
  };
}

#endif 
//____________________________________________________________________
//
// EOF
//


  
