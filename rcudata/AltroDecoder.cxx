//____________________________________________________________________
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Tue Jun 27 09:26:08 2006
    @brief   Implementation of ALTRO decoder
*/
#include "AltroDecoder.h"
#include "Channel.h"
#include "DebugGuard.h"
#include <TClonesArray.h>
#include <cmath>
#include <iostream>
#include <iomanip>

//____________________________________________________________________
const RcuData::AltroDecoder::w40_t 
RcuData::AltroDecoder::fgkTrailerMask = 
  ((RcuData::AltroDecoder::w40_t(0x2aaa) << 26) + 
   (RcuData::AltroDecoder::w40_t(0xa) << 12));

//____________________________________________________________________
RcuData::AltroDecoder::AltroDecoder()
  : fArray(0), 
    fDDL(0),
    fCurrent(0), 
    fCur(0), 
    fStart(0), 
    fN(0),
    fDecoded(0), 
    fDecodedCur(0), 
    fNEOD(0)
{
  DGUARD("ctor");
  fArray = new TClonesArray("RcuData::Channel");
}

//____________________________________________________________________
void
RcuData::AltroDecoder::Clear()
{
  DGUARD("All structires cleared");
  fArray->Clear();
  fCurrent = 0;
  fNEOD    = 0;
  // for (ChannelMap::iterator i = fMap.begin(); i != fMap.end(); ++i) 
  //   delete  i->second;
  // fMap.clear();
}

//____________________________________________________________________
void
RcuData::AltroDecoder::DecodeError(int err)
{
  DGUARD("Decoding the error %d", err);
  switch (err) {
  case kEOD:        std::cerr << "End of data"       << std::endl; break;
  case kRead:	    std::cerr << "Inconsistent read" << std::endl; break;
  case kBadValue:   std::cerr << "Invalid value"     << std::endl; break;
  case kBadTrailer: std::cerr << "Not a trailer"     << std::endl; break;
  case kBadFill:    std::cerr << "Not a fill word"   << std::endl; break;
  case kTooLong:    std::cerr << "Read too long"     << std::endl; break;
  default:          std::cerr << "Unknown error " << err << std::endl; break;
  }
}

    
//____________________________________________________________________
bool
RcuData::AltroDecoder::DecodeEvent(const Word32_t* data,
				   const Word32_t  size) 
{
  DGUARD("Decoding event from payload %p of size %d", data,size);
  Clear();

  size_t nw40   = 0;
  size_t lsize  = sizeof(uint32_t);
  size_t remain = 0;
  
  if ((data[size-1] & 0xFFFF0000) == 0xAAAA0000) {
    // We have RCU II formatted data 
    size_t   nw_trailer = (data[size-1] >> 0) & 0x7F;
    uint16_t rcu_id     = (data[size-1] >> 7) & 0x1FF;
    nw40                = (data[size-nw_trailer]);
    DMESG("Trailer information: \n"
	  " # trailer words:  %d\n" 
	  " # of 40bit words: %d\n"
	  " RCU Id:           0x%03x\n",
	  nw_trailer, nw40, rcu_id);
    for (size_t i = size-nw_trailer+1; i < size-1; i++) { 
      uint16_t param = (data[i] >> 26) & 0x7F;
      uint32_t value = (data[i] >>  0) & 0x1FFFFFF;
      DMESG(" Parameter 0x%02x:   0x%07x", param, value);
    }
  }
  else { 
    // Figure out where our data stops. 
    nw40   = size * lsize / 5;
    remain = size * lsize * 8 - nw40 * 40;

    // If we got less than a full word in the remainder, then the last
    // 40 it word is a partial extra word, so we disregard that. 
    if (remain < 32) nw40--;
  }
  fCur          = (uint8_t*)(&(data[size]));
  fStart        = (uint8_t*)(&(data[0]));
  fN            = 0;

  // Read in the extra words.  Nothing is done to them. 
  size_t nextra = size * lsize - nw40 * 40 / 8;
  // std::vector<unsigned char> extra;
  for (size_t i = 0; i < nextra; i++) {
    fCur--;
    // unsigned char t = *fCur;
    // extra.push_back(t);
  }

  int ret;
  while ((ret = DecodeChannel()) > 1);
  if (ret < 0 && ret != -kEOD) {
    std::cerr << "DecodeChannel returned " << ret << std::endl;
    DecodeError(-ret);
    return false;
  }
  return true;
}

//____________________________________________________________________
int
RcuData::AltroDecoder::GetNext10bitWord() 
{
  //  DGUARD("get the next 10bits");
  if (fDecoded) {
    if (fDecodedCur == 0) return -1;
    fDecodedCur--;
    return ((*fDecoded)[fDecodedCur]) & 0x3ff;
  }
  
  static w40_t  last40;
  if (fCur <= fStart && fN == 0) { 
    if (fNEOD > 0) return -kTooLong;
    fNEOD++;
    return -kEOD;
  }
  fNEOD = 0;
  

  if (fN == 0) {
    last40 = 0;
    fN     = 4;
    // Read in a 40bit word 
    for (int i = 4; i >= 0; i--) {
      fCur--;
      if (fCur <= fStart) return kRead;
      last40 += (w40_t(*fCur & 0xff) << (i * 8));
    }
  }
  fN--;
  int ret = (last40 >> (fN * 10)) & 0x3ff;
  if (ret < 0 || ret > 0x3ff) return kBadValue;
  return ret;
}

//____________________________________________________________________
int 
RcuData::AltroDecoder::DecodeTrailer(uint32_t& addr, uint32_t& last)
{
  DGUARD("decode trailer with address 0x%04x of size %d", addr, last);
  w40_t trailer = 0;

  int ret = 0;
  for (size_t i = 4; i > 0; i--) {
    int data =  GetNext10bitWord();
    if (data < 0) return data;
    trailer  += (w40_t(data)) << ((i-1)*10);
    ret++;
    // std::cout << "Trailer word " << i << " 0x" << std::hex 
    //  << std::setw(3) << data << std::dec << std::endl;
  }
  if (!IsTrailer(trailer)) {
    std::cerr << "Word  0x" << std::hex << std::setw(10) 
	      << std::setfill('0') << trailer << " (0x" 
	      << fgkTrailerMask << " 0x" << std::setw(10) 
	      << (trailer  & fgkTrailerMask) << ")"
	      << std::dec << std::setfill(' ')
	      << " does not fit a trailer"  << std::endl;
    return -kBadTrailer;
  }
  addr           =  (trailer & 0xfff);
  last           =  (trailer >> 16) & 0x3ff;
  size_t nfill   = (last % 4 == 0 ? 0 : 4 - last % 4);
  for (size_t i = 0; i < nfill; i++) {
    int fill = GetNext10bitWord();
    ret++;
    if (fill != 0x2aa) {
      uint32_t board, chip, channel;
      DecodeAddress(trailer, board, chip, channel);
      std::cerr << "Invalid fill 0x" <<  std::hex << fill << std::dec 
		<< " at " << i << " b" 
		<< std::setfill('0') 
		<< std::setw(2) << board   << "a" 
		<< std::setw(1) << chip    << "c" 
		<< std::setw(2) << channel 
		<< std::setfill(' ') << " w/" 
		<< std::setw(4) << last << " 10bit words"  << std::endl;
      // return -kBadFill;
    }
  }
  return ret;
}

//____________________________________________________________________
void
RcuData::AltroDecoder::DecodeAddress(const uint32_t addr, uint32_t& board, 
				     uint32_t& chip, uint32_t& channel) const
{
  DGUARD("Hardware address is 0x%04x", addr);
  channel  =  (addr >>  0) & 0x00f;
  chip     =  (addr >>  4) & 0x007;
  board    =  (addr >>  7) & 0x01f;
}  

//____________________________________________________________________
void
RcuData::AltroDecoder::GotChannel(uint32_t ddl, 
				  uint32_t board, 
				  uint32_t chip, 
				  uint32_t channel, 
				  uint32_t last)
{
  DGUARD("Got channel 0x%02x/0x%x/0x%02x (%d)", board, chip, channel, last);
  size_t index       = fArray->GetEntriesFast();
  fCurrent           = new ((*fArray)[index]) Channel;
  fCurrent->fDDL     = ddl;
  fCurrent->fBoard   = board;
  fCurrent->fChip    = chip;
  fCurrent->fChannel = channel;
  fCurrent->fLast    = last;
}

//____________________________________________________________________
void
RcuData::AltroDecoder::GotData(uint32_t t, uint32_t adc)
{
  // DGUARD("data at time=%4d is 0x%03x", t, adc);
  if (!fCurrent) { 
    std::cerr << "No current" << std::endl;
    return;
  }
  if (t < fCurrent->fM) fCurrent->fM = t;
  if (t > fCurrent->fN) fCurrent->fN = t;
  fCurrent->fData[t] = adc;
}

//____________________________________________________________________
int
RcuData::AltroDecoder::DecodeChannel(const Word10Vector_t& data, 
				     volatile size_t& size)
{
  DGUARD("decode channel @ from %p of size %d", &data, size);
  fDecoded    = &data;
  fDecodedCur = size;
  int ret     = DecodeChannel();
  fDecoded    = 0;
  return ret;
}

//____________________________________________________________________
int
RcuData::AltroDecoder::DecodeChannel()
{
  DGUARD("decode a channel");
  uint32_t addr, channel, chip, board, last;
  int ret  = 0;
  fCurrent = 0;
  if ((ret = DecodeTrailer(addr, last)) < 0) {
    if (ret != -kEOD) 
      std::cerr << "RcuData::AltroDecoder::DecodeChannel() failed at trailer" 
		<< std::endl;
    return ret;
  }
  DecodeAddress(addr, board, chip, channel);
  this->GotChannel(fDDL, board, chip, channel, last);
  
  int rem   = 0;
  int t     = 0;
  for (size_t i = 0; i < last; i++) {
    if (rem == 0) {
      // New bunch 
      rem =  GetNext10bitWord()-2;
      if (rem < 0) return rem;
      t   =  GetNext10bitWord();
      if (t < 0) return t;
      i   += 2;
      ret += 2;
    }
    t--;
    int data = GetNext10bitWord();
    if (data < 0) return data;
    ret++;
    this->GotData(t, uint32_t(data));
    rem--;
  }
  // std::cout << "Returnig " << ret << std::endl;
  return ret;
}

//____________________________________________________________________
//
// EOF
//
