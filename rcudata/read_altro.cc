//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
//____________________________________________________________________
/** @defgroup read_altro Program to read ALTRO data files 

    Program to read ALTRO formated files.  This reads the the files
    backward.  This is needed, as 3 fill words (3 0x2aa) may be
    mistaken for a trailer.  On the other hand, 3 x 0x2aa may be a
    valid trailer (last time would be 0x2aa=682), so one can not just
    check the next 40bit word.  The only solution, with the current
    ALTRO format, is to read from the back.  It makes the reading a
    bit harder, as this program illustrates.

    @author Christian Holm Christensen <cholm@nbi.dk>
*/
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <vector>
#include <list>
#include <stdexcept>
#include <sstream>

//____________________________________________________________________
/** @struct bunch 
    @ingroup read_altro
    @brief A bunch of data.  That is a sequence of signals followed by
    the time and size.  
*/
struct bunch
{
  /** The type of the internal container */
  typedef std::vector<unsigned short> signal_list;
  /** Iterator over the signals */
  typedef signal_list::iterator       iterator;
  /** Constant iterator over the signals */
  typedef signal_list::const_iterator const_iterator;
private:
  /** Array of the signals */
  unsigned short* _signals;
  /** The number of signals in this bunch */
  size_t          _n;
  /** The time of the first signal in the bunch */
  unsigned short _time;
  /** Check the index @a t if it's out of bounds.  Valid indicies are
      in the range @f$ [ @c _time, @c _time + @c _n) @f$.
      @exception std::domain_error If the index is out of bounds 
      @param t the index to test 
      @return  @c true if within bounds */
  bool check_bounds(size_t t) const
  {
    if (t < _time || t >= _time + _n) {
      std::stringstream s("Time ");
      s << std::dec << t << " is out of bounds, valid range is [" 
	<< _time << "," << _time + _n << ")";
      throw std::domain_error(s.str().c_str());
      return false;
    }
    return true;
  }
public:
  /** Constructor of a bunch. 
      @param l The number of signals + 2 
      @param t The time of the last signal in the bunch  */
  bunch(const unsigned short l, const unsigned short t) 
    : _signals(0), _n(l-2), _time(t - l + 3)
  { 
    _signals = new unsigned short[_n]; 
  }
  /** Destruct the bunch */
  virtual ~bunch() 
  {
    delete [] _signals;
  }
  /** Add a signal @a s at the time @a t 
      @param t The time 
      @param s The signal */
  void add_signal(size_t t, unsigned short s)
  {
    check_bounds(t);
    _signals[t - _time] = s; 
  }
  /** The number of signals in the bunch
      @return number of signals in the bunch */
  size_t size() const { return _n; }
  /** Access the signals
      @param t Time at which to get the signal. 
      @return The signal at time @a t */
  unsigned short& operator[](size_t t) 
  {
    check_bounds(t);
    return _signals[t - _time];
  }
  /** Access the signals
      @param t Time at which to get the signal. 
      @return The signal at time @a t */
  const unsigned short& operator[](size_t t) const
  {
    check_bounds(t);
    return _signals[t - _time];
  }
  /** Print the bunch to @c std::cout  */
  void print() const 
  {
    size_t n = _n;
    std::cout << "  Bunch of " << n << " signals, ending at t=" 
	      << _time << std::flush;
    for (size_t i = _time; i < _time + n; i++) {
      if ((i - _time) % 6 == 0) std::cout << std::endl;
      std::cout << "  " 
		<< std::setw(4) << std::setfill(' ') << std::dec << i << ":" 
		<< std::setw(4) << _signals[i - _time] << " " << std::flush;
    }
    std::cout << std::endl;
  }
};
  
//____________________________________________________________________
/** @struct channel 
    @ingroup read_altro
    @brief Channel information.   This contains information on which
    channel in which ALTRO chip, as well as the total number of 10bit
    words, and the two fill words, as well as a list of bunches. 
 */
struct channel
{
private:
  /** Channel number in the ALTRO chip */
  unsigned short _channel;
  /** The ALTRO chip number */
  unsigned short _chip;
  /** The number of 10bit words in the channel readout */
  unsigned short _last;
  /** Pattern word */
  unsigned short _a;
  /** Pattern word */
  unsigned short _fill;

  /** Type list of bunches */
  typedef std::list<bunch*> bunch_list;
  /** List of bunches */
  bunch_list _bunches;
public:
  /** Construct a channel 
      @param w The trailer 40bit word  */
  channel(const unsigned long long& w) 
  {
    _channel = w & 0xf;
    _chip    = (w >> 4) & 0xff;
    _a       = (w >> 12) & 0xf;
    _last    = (w >> 16) & 0x3ff;
    _fill    = (w >> 26) & 0x3fff;
  }
  /** Destruct the channel */
  virtual ~channel() 
  {
    for (bunch_list::iterator i = _bunches.begin(); i != _bunches.end(); ++i)
      delete (*i);
  }
  /** @return Get the channel number in the ALTRO chip */
  unsigned short chan()    const { return _channel; }
  /** @return Get the ALTRO chip number  */
  unsigned short chip()    const { return _chip; }
  /** @return Get the number of 10bit words  */
  unsigned short last()    const { return _last; }
  /** @return Get fill pattern (0xA)  */
  unsigned short a()       const { return _a; }
  /** @return Get fill pattern (0x2AAA)  */
  unsigned short fill()    const { return _fill; }
  /** Add a bunch to the channel information 
      @param b Bunch to add */
  void           add_bunch(bunch* b) { _bunches.push_front(b); }
  /** Print the channel information to @c std::cout */
  void           print() const 
  {
    std::cout << "Channel: address=" << std::setw(6) << _chip 
	      << "/" << std::setw(2) << _channel  
	      << " last=" << std::setw(4) << std::dec << _last 
	      << " a=0x" << std::hex << _a 
	      << " fill=0x" << std::hex << _fill 
	      << std::dec << std::endl;
    for (bunch_list::const_iterator i = _bunches.begin(); 
	 i != _bunches.end(); ++i) 
      (*i)->print();
  }
};

//____________________________________________________________________
/** @struct rfile 
    @ingroup read_altro 
    @brief Class to read a stream formatted as ALTRO raw data,
    bakwards.  Note, that it's not possible to read such a stream
    forward.   The reason is, that 3 fill words (0x2AA) matches a
    trailer (0x2AAA + 10bit word + 0xA + HW address). 
 */
struct rfile 
{
  /** Type of 40bit word */
  typedef unsigned long long w40_t;
  /** Type of 10bit word */
  typedef unsigned int       w10_t;
  /** Type of list of channels */
  typedef std::list<channel*> channel_list;
private:
  /** Type of the stream */
  typedef std::istream stream_type;
  /** Type of a position in the stream */
  typedef stream_type::pos_type pos_type;
  /** The first 10bit word in the stream (at the top) */
  unsigned int _first;
  /** The stream to read from */
  stream_type& _stream;
  /** The current position in the stream */
  pos_type     _current;
  /** The beginning of the file (top) */
  pos_type     _begin;
  /** The current line number */
  size_t       _lineno;

  /** List of channels */
  channel_list _channels;
  /** Pattern that matches a trailer */
  const w40_t  _trailer_mask;

  /** Read back until we find a new line or the beginingof the file 
   */
  bool skip_to_newline(bool initial=false) 
  {
    do {
      _stream.seekg(-1, std::ios_base::cur);
      if (_stream.bad()) {
	std::cerr << "Stream is in a bad way" << std::endl;
	return false;
      }
      _current = _stream.tellg();
      if (_current == _begin) return false;
      char c = _stream.get();
      _stream.seekg(_current);
      if (c == '\n') { 
	_lineno++;
	break;
      }
    } while (_current != _begin);
    return true;
  }

  /** Concatenate a10bit word into a 40bit word 
      @param n Number of 10bit word in the 40bit word (0-3)
      @param w The 40bit word to update 
      @return The 40bit word to update */
  w40_t concat_w40(size_t n, const w10_t& w) const
  {
    if (n > 3) return 0;
    return w40_t(w & 0x3ff)  << 10 * n;
  }

  /** Extract the @a nth 10bit word from the 40bit word @a w
      @param n The offset of the 10bit word to extract (0-3)
      @param w The 40bit word to extract the 10bit word from
      @return The 10bit word at offset @a n */
  w10_t extract_w10(size_t n, const w40_t& w) const
  {
    if (n > 3) return 0;
    return (w >> 10 * n) & 0x3ff;
  }

  /** Get the next 10bit word when reading the bunch data.  That is,
      the class reads in one 40bit word at a time.  This member
      function then takes care of extracting the next 10bit word from
      the read-in 40bit word.  If needed, it reads in the next 40bi
      word from the stream. 
      @param offset On input, the previous offset in the current 40bit
      word, on output the current offset of the returned 10bit word. 
      @param w The current 40bit word.  If this is exhausted do to
      this member function returning the last 10bit word, then this
      argumented is updated to give the next 40bit word. 
      @param count Count down to zero.  On successfull read, this is
      decremented by one. 
      @return The next 10bit word. */
  w10_t get_w10(size_t& offset, w40_t& w, w10_t& count) 
  {
    if (offset > 3) {
      w = get_next_w40();
      // _lineno++;
      if (is_trailer(w)) {
	std::cerr << "Line "<< std::setw(3) << _lineno << " 0x"
		  << std::setfill('0')  << std::setw(10) << std::hex 
		  << w << " matches a trailer, but should be data" 
		  << std::dec << std::setfill(' ') << std::endl;
	return 0;
      }
      offset = 0;
    }
    w10_t retval = extract_w10(3 - offset, w);
    offset++;
    count--;
    return retval;
  }
  
  /** Extract the bunc information pertaining to the channel @a c from
      the stream.   This reads 40bit words from the stream until the
      number of 10bit words given in the trailer of @a c has been
      read.  This obviously checks if there's any fill words at the
      end of the data stream, and if so, check that they are valid. 
      @param c The channel to read bunches for.  On return, this
      contains the read bunches. 
      @return @c true on success, and @c false on errors  */
  bool extract_bunches(channel* c) 
  {
    w40_t w      = get_next_w40();
    w10_t last   = c->last();
    size_t nfill = (last % 4 == 0 ? 0 : 4 - last % 4);
    for (size_t i = 3; i >= 4 - nfill; i--) {
      w10_t f = extract_w10(i, w); 
      if (f != 0x2aa) {
	std::cerr << "Line "<< std::setw(3) << _lineno++   << ": 0x"
		  << std::setfill('0')  << std::hex << std::setw(10) 
		  << f << " is not a fill word (0x2aa)" 
		  << std::dec << std::setfill(' ') << std::endl;
	return false;
      }
    }
    size_t offset = nfill;
    while (last > 0)  {
      w10_t l = get_w10(offset, w, last);
      w10_t t = get_w10(offset, w, last);
      bunch* b = new bunch(l, t);
      for (size_t i = 0; i < l-2; i++) {
	int tt = t;
	w10_t s = get_w10(offset, w, last);
	b->add_signal(tt - i, s);
      }
      c->add_bunch(b);
    }
    return true;
  }
  
public:
  /** Construct an object to read ALTRO data from the stream @a s. 
      @param s The stream to read from. */
  rfile(std::istream& s) 
    : _stream(s), _lineno(0), 
      _trailer_mask((w40_t(0x2aaa) << 26) + (w40_t(0xa) << 12))
  {
    _stream.seekg(0, std::ios_base::beg);
    _begin   = _stream.tellg();
    _stream >> _first;
    _stream.seekg(0, std::ios_base::end);
    _current = _stream.tellg();
    skip_to_newline(true);
  }
  
  /** @return The next 10bit word from the file.  */
  w10_t get_next_w10() 
  {
    if (!skip_to_newline()) return _first;
    w10_t x;
    _stream >> x;
    _stream.seekg(_current);
    // _lineno++;
    return x;
  }

  /** @return The next 40bit word from the file.  */
  w40_t get_next_w40() 
  {
    w40_t w40 = 0;
    for (int i = 3; i >= 0; i--) {
      w10_t tmp = get_next_w10();
      w40 += concat_w40(i, tmp);
    }
    return w40;
  }
    
  /** @return @c true if the beginning of the file has been seen, @c
      false otherwise  */
  bool is_bof() const { return _current == _begin; }
  /** @return @c true if @c badbit has been set on the underlying
      stream, @c false otherwise  */
  bool is_fail() const { return _stream.fail() && !_stream.eof(); }
  /** Check if the 40bit word @a w matches the trailer pattern. 
      @param w 40bit word to check
      @return @c true if @a w matches the trailer pattern, @c false
      otherwise. */
  bool is_trailer(const w40_t& w) const 
  { 
    return ((w & _trailer_mask) == _trailer_mask);
  }

  /** Read the stream passes to the constructor. 
      @return @c true on success, @c false otherwise */
  bool read() 
  {
    do {
      w40_t t = get_next_w40();
      if (!is_trailer(t)) {
	std::cerr << "Line " << std::setw(3) << _lineno++  << ": 0x"
		  << std::hex << std::setw(10) << std::setfill('0') 
		  << t << " is not a trailer" 
		  << std::dec << std::setfill(' ') << std::endl;
	return false;
      }
      channel* c = new channel(t);
      _channels.push_front(c);

      if (c->last() == 0) continue;
      bool ret = extract_bunches(c);
      if (!ret) return false;
    } while(!is_bof());
    return true;
  }

  /** Print the read information.  */
  void print() const 
  {
    for (channel_list::const_iterator i = _channels.begin(); 
	 i != _channels.end(); ++i) 
      (*i)->print();
  }
};


//____________________________________________________________________
/** Program to read in one ALTRO data file, and dump the information
    to @c std::cout 
    @ingroup read_altro
    @param argc Number of command line arguments
    @param argv Vector of strings of the command line arguments. 
    @return 0 on success, non-zero on error. */
int main(int argc, char** argv) 
{
  try {
    std::string filename("test_data000");
    if (argc > 1) filename = argv[1];
    
    std::ifstream file(filename.c_str());
    if (!file) {
      std::cerr << "Couldn't open file \"" << filename << "\"" 
		<< std::endl;
      return 1;
    }

    rfile r(file);
    if (!r.read()) {
      std::cerr << "Failure" << std::endl;
      return false;
    }
    r.print();
    
    file.close();
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  
  return 0;
}

//____________________________________________________________________
//
// EOF
//
