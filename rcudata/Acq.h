// -*- mode: c++ -*-  
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUDATA_ACQ_H
#define RCUDATA_ACQ_H
#ifndef RCUXX_ACQ_H
# include <rcuxx/Acq.h>
#endif
#ifndef RCUDATA_ALTRODECODER_H
# include <rcudata/AltroDecoder.h>
#endif
#ifndef ROOT_TObject
# include <TObject.h>
#endif
#ifndef ROOT_RQ_OBJECT
# include <RQ_OBJECT.h>
#endif
class TTree;
class TFile;
class TClonesArray;

namespace RcuData
{
  /** @class Acq 
      @brief Abstract base class to do data aquisition with ROOT data
      output 
      @ingroup rcudata
   */
  class Acq : public Rcuxx::Acq, public AltroDecoder
  {
  public:
    /** Type of data array */
    // typedef Rcuxx::Acq::DataVector_t DataVector_t;
    typedef std::vector<uint10_t> DataVector_t;
    /** Type of data */
    // typedef Rcuxx::Acq::Data_t       Data_t;
    typedef uint10_t Data_t;
    /** Constructor
	@param rcu  Reference to RCU object 
	@param out  Output file name.  If left blank, an automatic
	name, based on the run number will be chosen. */
    Acq(Rcuxx::Rcu& rcu, const char* out="") 
      : Rcuxx::Acq(rcu, out), 
	AltroDecoder(),
	fFile(0),
	fTree(0) 
    {}
    /** Set up 
	@param nRun Run number
	@param nevents Number of events to process
	@param mode The trigger mode. 
	@param mask Mask of stuff to commit, 
	@param addr Address to execute */ 
    virtual unsigned int Setup(int          nRun=0, 
			       int          nevents=-1, 
			       Trigger_t    mode=kSoftwareTrigger,
			       unsigned int mask=(kACTFEC|kFECRST|kTRCFG1|
						  kIMEM|kPMEM|kACL), 
			       unsigned int addr=0);
    /** Run an acquisition
	@return 0 on success, error code otherwise */
    virtual unsigned int Run();
  protected:
    /** Write information about run to the output file */
    virtual unsigned int WriteInfo();
    /** Hook called at the beginning of an event, after accepting a
	trigger 
	@param evno The current event number */
    virtual int StartEvent(unsigned int evno);
    
    /** Hook called at end of an event
	@param evno The current event number */
    virtual int EndEvent(unsigned int evno); 
    
    /** Process the data from one ALTRO channel.  The full data from
	one ALTRO channel is passed as the argument @a data.  The
	array has  @a size elements.   The elements are 10bit words
	formated as ALTRO data.  That is, it has the format 
	<table>
        <tr><td>S01</td><td>S02</td><td>S03</td><td>S04</td></tr>
        <tr><td>S05</td><td>T05</td><td>007</td><td>S05</td></tr>
        <tr><td>S06</td><td>S07</td><td>S08</td><td>T08</td></tr>
        <tr><td>006</td><td>S09</td><td>...</td><td>...</td></tr>
        <tr><td>...</td><td>...</td><td colspan=2>0x2aa</td></tr>
	<tr><td>0x2aaa</td><td>L</td><td>0xa</td><td>address</td></tr>
	</table>
	@param data The ALTRO formated data 
	@param size The size of the array @a data
	@return 0 on success, error-code otherwise */
    virtual int ProcessChannel(const DataVector_t& data, 
			       unsigned int size);
    /** File */ 
    TFile* fFile;
    /** Tree */
    TTree* fTree;
    /** Array */ 
    // TClonesArray* fArray;
    
    ClassDef(Acq,0)
  };
}

#endif

  
