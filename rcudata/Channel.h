// -*- mode: C++ -*- 
//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
#ifndef RCUDATA_CHANNEL_H
#define RCUDATA_CHANNEL_H
#ifndef ROOT_TObject
# include <TObject.h>
#endif

class TGraph;
class TTree;
class TClonesArray;
class TH1;

/** @defgroup rcudata Data structures 
 */
/** @namespace RcuData RCU data structures for ROOT
    @ingroup rcudata
 */
namespace RcuData 
{
  /** @struct Header rcudata/Channel.h <rcudata/Channel.h>
      @brief Simple event header 
      @ingroup rcudata 
   */
  struct Header 
  {
    /** Event number */
    Int_t fEventNo;
    /** Number of channels in the event */
    Int_t fNChannels;
  };
  
  /** @class Channel rcudata/Channel.h <rcudata/Channel.h>
      @brief Channel data 
      @ingroup rcudata
  */
  class Channel : public TObject 
  {
  public:
    /** DDL number */
    UShort_t fDDL;
    /** Channel number */
    UShort_t fBoard;
    /** Channel number */
    UShort_t fChannel;
    /** Chip number */
    UShort_t fChip;
    /** Last 10bit word written */
    UShort_t fLast;
    /** First bin with data */
    UShort_t fM;
    /** Last bins with data */
    UShort_t fN;
    /** The data */
    UShort_t fData[1024];

    /** CTOR */
    Channel();
    /** CTOR 
	@param ddl  DDL number 
	@param data Data to read from 
	@param size Size of @a data */
    Channel(const UInt_t ddl, const UInt_t* data, UInt_t size);
    /** DTOR */
    virtual ~Channel();

    /** @return @c true if channel contains valid data */
    bool IsValid() const { return (fN > fM); }
    /** Get the DDL number */
    unsigned int DDL() const { return fDDL; }
    /** @return Board number  */
    unsigned int Board() const { return fBoard; }
    /** @return Chip number  */
    unsigned int Chip() const { return fChip; }
    /** @return Channel number  */
    unsigned int ChanNo() const { return fChannel; }
    /** @return Least time bin */ 
    unsigned int MinT() const { return fM; }
    /** @return Largets time bin */ 
    unsigned int MaxT() const { return fN; }

    /** Print the channel data 
	@param option Print options */
    void Print(Option_t* option="B") const;

    /** Draw this channel data in a graph 
	@param g Graph to draw in.  If it's null, then a new graph is
	made and returned to the user. */
    void PutInGraph(TGraph*& g) const;
    /** Draw this channel data in a histogram
	@param h    Histogram to draw in.  If it's null, create a new
	histogram and return it to the user. 
	@param tmin Minimum time bin
	@param tmax Maximum time bin
	@param off  Additional offset along X-axis in histogram */
    void PutInHist(TH1*& h, Int_t tmin, Int_t tmax, Double_t& off) const;

    /** Static function to draw channel spectrum data in a TTree in a
	graph. 
	@param tree     Tree to read from. 
	@param board    Specified board only (-1 selects all) 
	@param chip     Specified chip only (-1 selects all) 
	@param channel  Specified channel only (-1 selects all) 
	@param evno     Specified event only (-1 selects all events). 
	@return @c false in case of errors  */
    static Bool_t PlotSpectrum(TTree* tree, Int_t ddl=-1,
			       Int_t board=-1, Int_t chip=-1, 
			       Int_t channel=-1, Int_t evno=-1);

    /** Static function to draw channel time series data in a TTree in
	a graph. 
	@param tree     Tree to read from. 
	@param board    Specified board only (-1 selects all) 
	@param chip     Specified chip only (-1 selects all) 
	@param channel  Specified channel only (-1 selects all) 
	@param evno     Specified event only (-1 selects all events). 
	@param channels Array to read from.  If null, an internal
	array will be used. 
	@return the drawn graph, or @c 0 in case of errors  */
    static TGraph* PlotTimeSeries(TTree* tree, Int_t ddl=-1,
				  Int_t board=-1, Int_t chip=-1, 
				  Int_t channel=-1, Int_t evno=-1, 
				  TClonesArray* channels=0);
    /** Static function to draw channel time series data in a TTree in
	a histogram. 
	@param tree     Tree to read from. 
	@param board    Specified board only (-1 selects all) 
	@param chip     Specified chip only (-1 selects all) 
	@param channel  Specified channel only (-1 selects all) 
	@param evno     Specified event only (-1 selects all events). 
	@param tmin     Mimimum time bin to draw (-1 is select from tree)
	@param tmax     Mimimum time bin to draw (-1 is select from tree)
	@param channels Array to read from.  If null, an internal
	array will be used. 
	@return the drawn histogram, or @c 0 in case of errors  */
    static TH1* PlotTimeHist(TTree* tree, Int_t ddl=-1,
			     Int_t board=-1, Int_t chip=-1, 
			     Int_t channel=-1, Int_t evno=-1, Int_t tmin=-1, 
			     Int_t tmax=-1, TClonesArray* channels=0);
  
    ClassDef(Channel, 3)
  };
}

#endif
//
// EOF
//
