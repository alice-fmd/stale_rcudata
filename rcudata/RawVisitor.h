// -*- mode: C++ -*-
//____________________________________________________________________
//
//  Project to read DATE raw data
//  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
/** @file    RawVisitor.h
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Sun Jun 25 12:10:05 2006
    @brief   Declaration of ALTRO printer 
*/
#ifndef RCUDATA_RAWVISITOR_H
#define RCUDATA_RAWVISITOR_H
#ifndef READRAW_VISITOR_H
# include <readraw/Visitor.h>
#endif
#ifndef READRAW_EQUIPMENT_H
# include <readraw/Equipment.h>
#endif
#ifndef RCUDATA_ALTRODECODER_H
# include <rcudata/AltroDecoder.h>
#endif

class TTree;
class TFile;

namespace RcuData
{
  class ProgressMeter;
  
  /** Print altro events */
  struct RawVisitor : public ReadRaw::Visitor, public AltroDecoder
  {
    /** Contructor.  
	@param output Stream to write to. 
	@param progress Make progress bar  */
    RawVisitor(const char* output=0, bool progress=true);
    /** Destructor */
    virtual ~RawVisitor();
    /** Print contents of event, and possible sub-events. 
	@param event Event to print
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const ReadRaw::Event& event); 
    /** Visit an equipment.  Calls Visit on the common data header,
        and the data of the equipment. 
        @param equipment Equipment object to visit.
        @return @c true on success, @c false otherwise */
    virtual bool Visit(const ReadRaw::Equipment& equipment);
    /** Print the raw data from an equipment. 
	@param data Raw data from the equipment, packed in 32bit
	words. 
	@param size Number of 32bit words in @a data 
	@return @c true on success, @c false otherwise */
    virtual bool Visit(const uint32_t* data, 
		       const uint32_t  size);
    /** Current estimate of number of events */
    virtual unsigned long EstimatedNEvents() const;
    /** Set the estimate of number of events */
    virtual void SetEstimatedNEvents(uint32_t n);
  protected:
    /** output file */
    TFile* fFile;
    /** Output tree */
    TTree* fTree;
    /** Progress meter */
    ProgressMeter* fMeter;
  };
}

#endif
//
// EOF
//

  
