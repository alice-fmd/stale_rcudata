//
// Copyright (C) 2006 Christian Holm Christensen <cholm@nbi.dk>
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//
/** @file    Spectra.cxx
    @author  Christian Holm Christensen <cholm@nbi.dk>
    @date    Mon Jul 24 00:20:14 2006
    @brief   
    @ingroup rcudata_spectra
*/
#include "rcudata/Spectra.h"
// #include <rcuxx/DebugGuard.h>
#include <rcudata/ProgressMeter.h>
#include <TObjArray.h>
#include <TDirectory.h>
#include <TH1.h>
#include <iostream>
#include <iomanip>
#include <cassert>

namespace 
{
  RcuData::ProgressMeter fgWriteMeter;
}

namespace RcuData 
{
  namespace Spectra
  {    
    //==================================================================
    Top::Top(unsigned int id) : Base_t(id)
    {
      MakeSummary(1024, -.5, 1023.5);
      fSummary->SetFillColor(6);
      fSummary->SetName("summed");
      fSummary->SetTitle("Summary of all timebins");
    }
    //________________________________________________________________
    const char* Top::GetName() const 
    {
      return Form("top_%01d", fId);
    }
    //________________________________________________________________
    Top::Elem_t* Top::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Top::WriteOut() 
    {
      fgWriteMeter.Reset(Count(),"Writing");
      TDirectory* savDir = gDirectory;
      for (Base_t::Iter_t i = Begin(); i != End(); ++i) 
	WriteElem(i->second);
      if (fSummary) fSummary->Write();
      std::cout << std::endl;
      if (savDir) savDir->cd();
    }
    //________________________________________________________________
    void Top::WriteElem(Elem_t* e) 
    { 
      if (!e) {
	std::cerr << "No rcu to write" << std::endl;
	return;
      }
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Top::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Top::Fill(unsigned int rcu,  unsigned int board,   
		   unsigned int chip, unsigned int channel, 
		   unsigned int t,    unsigned int adc) 
    {
      if (fSummary) {
	fSummary->AddBinContent(adc+1);
	fSummary->SetEntries(fSummary->GetEntries()+1);
      }
      Spectra::Rcu* r = GetOrAdd(rcu);
      if (!r) return;
      r->Fill(board, chip, channel, t, adc);
    }
    //==================================================================
    Rcu::Rcu(unsigned int id, Top& top) 
      : Base_t(id), 
	fTop(&top)
    {
      MakeSummary(1024, -.5, 1023.5);
      fSummary->SetFillColor(6);
      fSummary->SetName("summed");
      fSummary->SetTitle("Summary of all timebins");
    }
    //________________________________________________________________
    const char* Rcu::GetName() const 
    {
      return Form("rcu_%01d", fId);
    }
    //________________________________________________________________
    Rcu::Elem_t* Rcu::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Rcu::WriteElem(Elem_t* e) 
    { 
      if (!e) {
	std::cerr << "No board to write" << std::endl;
	return;
      }
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Rcu::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Rcu::Fill(unsigned int board,   unsigned int chip, 
		   unsigned int channel, unsigned int t, 
		   unsigned int adc) 
    {
      if (fSummary) {
	fSummary->AddBinContent(adc+1);
	fSummary->SetEntries(fSummary->GetEntries()+1);
      }
      Spectra::Board* b = GetOrAdd(board);
      if (!b) return;
      b->Fill(chip, channel, t, adc);
    }
    
    //==================================================================
    Board::Board(unsigned int id, Rcu& rcu) 
      : Base_t(id), 
	fRcu(&rcu) 
    {
      MakeSummary(1024, -.5, 1023.5);
      fSummary->SetFillColor(7);
      fSummary->SetName(Form("b%02d", fId));
      fSummary->SetTitle(Form("Summary of all time bins in board %2d", fId));
    }
    //________________________________________________________________
    const char* Board::GetName() const 
    {
      return Form("board_%02d", fId);
    }
    //________________________________________________________________
    Board::Elem_t* Board::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Board::WriteElem(Elem_t* e) 
    { 
      if (!e) {
	std::cerr << "No chip to write out" << std::endl;
	return;
      }
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Board::CountElem(Elem_t* e) const { return e->Count(); }
    
    //________________________________________________________________
    void Board::Fill(unsigned int chip, unsigned int channel, unsigned int t, 
		     unsigned int adc) 
    {
      if (fSummary) {
	fSummary->AddBinContent(adc+1);
	fSummary->SetEntries(fSummary->GetEntries()+1);
      }
      Spectra::Chip* c = GetOrAdd(chip);
      if (!c) return;
      c->Fill(channel, t, adc);
    }
  
    //==================================================================
    Chip::Chip(unsigned int n, Board& b) 
      : Base_t(n), 
	fBoard(&b) 
    {
      MakeSummary(1024, -.5, 1023.5);
      fSummary->SetFillColor(3);
      fSummary->SetName(Form("b%02d_a%d", fBoard->Id(), fId));
      fSummary->SetTitle(Form("Summary of all time bins in board %2d, "
			      "chip %d", fBoard->Id(), fId));

    }
    //________________________________________________________________
    const char* Chip::GetName() const 
    {
      return Form("chip_%01d", fId);
    }
    //________________________________________________________________
    Chip::Elem_t* Chip::GetOrAdd(unsigned int id) 
    {
      if (!fCont[id]) fCont[id] = new Elem_t(id, *this);
      return fCont[id];
    }
    //________________________________________________________________
    void Chip::WriteElem(Elem_t* e) 
    { 
      if (!e) {
	std::cerr << "No channel to write out " << std::endl;
	return;
      }
      e->WriteOut(); 
    }
    //________________________________________________________________
    size_t Chip::CountElem(Elem_t* e) const { return e->Count(); }

    //________________________________________________________________
    void Chip::Fill(unsigned int channel, unsigned int t, 
		    unsigned int adc) 
    {
      if (fSummary) {
	fSummary->AddBinContent(adc+1);
	fSummary->SetEntries(fSummary->GetEntries()+1);
      }
      Spectra::Chan* c = GetOrAdd(channel);
      if (!c) return;
      c->Fill(t, adc);
    }

    //==================================================================
    Chan::Chan(unsigned int id, Chip& c) 
      : Container<TH1>(id), 
	fChip(&c)
    {
      size_t chip  = fChip->Id();
      size_t board = fChip->Mother().Id();
      MakeSummary(1024, -.5, 1023.5);
      fSummary->SetTitle(Form("Summary of all time bins in board %2d, "
			      "chip %d, channel %2d", board, chip, fId));
      fSummary->SetName(Form("b%02d_a%d_c%02d", board, chip, fId));
      fSummary->SetFillColor(4);
    }
    
    //________________________________________________________________
    const char* Chan::GetName() const 
    {
      return Form("channel_%01d", fId);
    }
    //________________________________________________________________
    size_t Chan::Count() const
    {
      size_t ret = fCont.size();
      return ret;
    }

    //________________________________________________________________
    TH1* Chan::GetOrAdd(unsigned int t) 
    {
      if (!fCont[t]) {
	size_t chip  = fChip->Id();
	size_t board = fChip->Mother().Id();
	std::string name(Form("b%02d_a%d_c%02d_t%04d", board, chip, fId, t));
	std::string title(Form("ADC Spectrum for Board %d, Chip %d, " 
			       "Channel %2d, Time-bin %4d", 
			       board, chip, fId, t));
	// Info("Fill", "Making histogram %s", name.c_str());
	fCont[t] = new TH1I(name.c_str(), title.c_str(), 1024, -.5, 1023.5);
	fCont[t]->SetXTitle("ADC counts");
	fCont[t]->SetYTitle("Events");
	fCont[t]->SetFillColor(2);
	fCont[t]->SetFillStyle(3001);
	fCont[t]->SetDirectory(0);
      }
      return fCont[t];
    }
    
    //________________________________________________________________
    void Chan::Fill(short s, unsigned int adc) 
    {
      TH1* h = GetOrAdd(s);
      h->AddBinContent(adc+1);
      h->SetEntries(fCont[s]->GetEntries()+1);
      if (fSummary) {
	fSummary->AddBinContent(adc+1);
	fSummary->SetEntries(fSummary->GetEntries()+1);
      }
      // fCont[s]->Fill(adc);
    }
    //__________________________________________________________________
    TH1* Chan::GetSpectrum(short sample) 
    {
      return fCont[sample];
    }
    //__________________________________________________________________
    void Chan::WriteOut() 
    {
      TDirectory* savDir = gDirectory;
      std::string name(GetName());
      if (!gDirectory->GetDirectory(name.c_str())) 
	gDirectory->mkdir(name.c_str())->cd();
      else 
	gDirectory->cd(name.c_str());
      for (Iter_t i = Begin(); i != End(); ++i) {
	fgWriteMeter.Step();
	WriteElem(i->second);
      }
      if (fSummary) fSummary->Write();
      savDir->cd();
    }

    //__________________________________________________________________
    void Chan::WriteElem(Elem_t* e) 
    {
      if (!e) {
	std::cout << "No spectrum to write out" << std::endl;
	return;
      }
      e->Write();
    }
    
    //__________________________________________________________________
    void
    Chan::Reset() 
    {
      Container<TH1>::Reset();
      if (fSummary) fSummary->Reset();
    }
    //__________________________________________________________________
    void
    Chan::Clear() 
    {
      Container<TH1>::Clear();
      if (fSummary) delete fSummary;
    }
  }
}
//
// EOF
//
