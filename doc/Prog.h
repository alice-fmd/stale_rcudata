/*
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Prog.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Acquisition programs. */
/** @page prog Acquisition programs

    There's a small program called @c rootacq which will run a
    data acquisition in batch mode.

    @verbatim
    Usage: rootacq [OPTIONS]
    
    Options:
    
            -h      This help
            -t N    Number of ALTRO timebins   [255]
            -n N    Number of events           [10]
            -d N    Set Rcu++ debug level to N [0]
            -u N    Set U2F debug level to N   [0]
            -a N    Set ALTRO debug level to N [0]
            -r N    Run number                 [0]
            -s      Software trigger           [false]
            -x      External trigger           [true]
            -e      Emulation mode             [false]
      Options specific for FMD:
            -f      Use FMD Acq                [true]
            -o N    Over sampling ratio        [2]
            -P N    Pulser size                [0]
            -m N    Minimum strip              [0]
            -M N    Maximum strip              [127]
    
      when using FMD acq., the number of timebins is ignored
    
    @endverbatim

    @b Note: that this program resets the FEC before running the
    data acquisition, so any changes  you may have done in the GUI
    is lost.  

    The specialised program @c fmdscan makes a scan over the
    biases and other parameters of the VA1.

    @verbatim
    Usage: fmdscan [OPTIONS]
    
    Options:
            -h      This help
            -t N    Number of ALTRO timebins [255]
            -n N    Number of events         [10]
            -d N    Set debug level to N     [0]
            -p N    Set debug package ID N   [0]
            -o N    Over sampling ratio      [2]
            -s      Software trigger         [false]
            -x      External trigger         [true]
            -e      Emulation mode           [false]
            -S MIN MAX STEP Strip range      [0,1,1]
            -H MIN MAX STEP Hold range       [32,64,16]
            -P MIN MAX STEP Pulser range     [0,255,55]
            -B MIN MAX STEP Shaping bias     [191,223,16]
            -V MIN MAX STEP VFS range        [103,135,16]
            -W MIN MAX STEP VFP range        [95,127,16]
    @endverbatim
    
    @b Note: that this program resets the FEC before running the
    data acquisition, so any changes you may have done in the GUI
    is lost. 

*/

#error Not for compilation
//
// EOF
//
