/*
 * RCU compiler
 * Copyright (c) 2004 Christian Holm <cholm@nbi.dk>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License 
 * as published by the Free Software Foundation; either version 2 of 
 * the License, or (at your option) any later version.  
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details. 
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
 * MA 02111-1307  USA  
 *
 */
/** @file   Viz.h
    @author Christian Holm
    @date   Sun Sep 15 16:29:24 2002
    @brief  Visualization documentation. */
/** @page viz Visualising the data

    Where ever you stored your data, you can visiualise the data
    using ROOT some  pre-build code.

    @section pedmak  Making Pedestal summaries

    To make plots of the pedestals and the noise, do
    @code
    prompt% root
    Root> gSystem->Load("librcudata.so");
    Root> RcuData::PedestalMaker pm("00000.root",
                                    "w/pulser @ 0x7f",
                                    -1, 0, 200);
    Root> pm.Exec("");
    @endcode

    The arguments to the RcuData::PedestalMaker constructor is 
    - ROOT file to analyse.
    - Title to put on the plots (please write something meaningful
      here – it helps you later to identify the data).
    - Number of events to analyse (less than zero means all
      events).
    - Number of events to skip from the beginning of the file.
    - Spacing between channels in overview plots.

    The ADC spectra and summary plots are written to a file named 
    @c hist_< @e input_file @c > where @c < @e input_file @c > is 
    the name of the input file (1st argument above).

    The string argument to RcuData::PedestalMaker::Exec is an option
    string.  If it contains the word @c fit (any case), then a
    Guassian distribution will be fitted to the pedestal ADC
    spectra, and summary plots of the @f$\mu@f$ and @f$\sigma@f$
    will be made.  Note, that a summary histogram of
    @f$\chi^2/NDF@f$ is also made.

    Once the summary plots are shown, one can zoom in on specific 
    channels (VA1 chip) and specific strips, by doing
    @code 
    Root> pm.Zoom(3, 0, 64);
    @endcode 

    Here the arguments are
    - The channel (VA1 chip) to zoom on (counting from 0). 
    - The first strip to show (counting from 0).
    - The last strip to show (counting from 0).

    @b Note: that each time a new plot is made on the screen, a 
    postscript file with the corresponding contents is created in
    the working directory.

    @section scripts Scripts to show other aspects of the data

    There are some scripts to show the low level data as well.
    These are:
    - @c ShowSpectrum.C Show spectrum for one or all channels
    - @c ShowTimeSeries.C Show timeseries for one or all
    channels

    You can run these scripts from ROOT, by doing
    @verbatim 
    prompt% root
    Root> .x ShowSpectrum.C(0, -1, -1, -1);
    @endverbatim

*/

#error Not for compilation
//
// EOF
//
